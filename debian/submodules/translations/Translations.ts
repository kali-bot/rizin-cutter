<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="14"/>
        <source>About Cutter</source>
        <translation>À propos de Cutter</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="69"/>
        <source>Check for updates on start</source>
        <translation>Vérifier les mises à jour au démarrage</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="95"/>
        <source>Show version information</source>
        <translation>Afficher les informations de version</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="108"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by Rizin&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">Afficher les informations du plugin radare2</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter is a free and open-source reverse engineering platform powered by radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Read more on &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;cutter.re&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;Cutter est une plate-forme de reverse engineering et open-source développé par radare2&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;En savoir plus sur &lt;/span&gt;&lt;a href=&quot;https://cutter.re&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;coupeur. e&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:28pt; font-weight:600;&quot;&gt;Cutter&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Show plugin information</source>
        <translation type="vanished">Afficher les informations du plugin</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="82"/>
        <source>Check for updates</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="28"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Using r2-</source>
        <translation type="vanished">Utilisant r2-</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="28"/>
        <source>Using rizin </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="30"/>
        <source>Optional Features:</source>
        <translation>Fonctionnalités optionnelles:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="47"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="48"/>
        <source>This Software is released under the GNU General Public License v3.0</source>
        <translation>Ce programme est distribué sous licence GNU General Public License v3.0</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="49"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="50"/>
        <source>Cutter is developed by the community and maintained by its core and development teams.&lt;br/&gt;</source>
        <translation>Cutter est développé par la communauté et maintenu par ses équipes de base et de développement.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="52"/>
        <source>Check our &lt;a href=&apos;https://github.com/rizinorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="76"/>
        <source>Rizin version information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check our &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;contributors page&lt;/a&gt; for the full list of contributors.</source>
        <translation type="vanished">Consultez notre &lt;a href=&apos;https://github.com/radareorg/cutter/graphs/contributors&apos;&gt;page de contributeurs&lt;/a&gt; pour la liste complète des contributeurs.</translation>
    </message>
    <message>
        <source>radare2 version information</source>
        <translation type="vanished">afficher les informations de version de radare2</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="99"/>
        <source>Checking for updates...</source>
        <translation>Recherche de mises à jour...</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="109"/>
        <source>Cutter is up to date!</source>
        <translation>Cutter est à jour !</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="150"/>
        <source>Based on Qt %1 (%2, %3 bit)</source>
        <translation>Basé sur Qt %1 (%2, %3 bits)</translation>
    </message>
    <message>
        <source>Timeout error!</source>
        <translation type="vanished">Erreur de délai d’attente&#xa0;!</translation>
    </message>
    <message>
        <source>Please check your internet connection and try again.</source>
        <translation type="vanished">Veuillez vérifier votre connexion internet et réessayer.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="105"/>
        <source>Error!</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDialog.cpp" line="108"/>
        <source>Version control</source>
        <translation>Contrôle de version</translation>
    </message>
    <message>
        <source>You have latest version and no need to update!</source>
        <translation type="vanished">Vous avez la dernière version et aucun besoin de mettre à jour&#xa0;!</translation>
    </message>
    <message>
        <source>Current version:</source>
        <translation type="vanished">Version actuelle :</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation type="vanished">Dernière version :</translation>
    </message>
    <message>
        <source>For update, please check the link:</source>
        <translation type="vanished">Pour mettre à jour, veuillez consulter le lien :</translation>
    </message>
</context>
<context>
    <name>AddressableDockWidget</name>
    <message>
        <location filename="../widgets/AddressableDockWidget.cpp" line="12"/>
        <source>Sync/unsync offset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddressableItemContextMenu</name>
    <message>
        <location filename="../menus/AddressableItemContextMenu.cpp" line="16"/>
        <source>Show in</source>
        <translation>Afficher dans</translation>
    </message>
    <message>
        <location filename="../menus/AddressableItemContextMenu.cpp" line="17"/>
        <source>Copy address</source>
        <translation>Copier l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../menus/AddressableItemContextMenu.cpp" line="18"/>
        <source>Show X-Refs</source>
        <translation>Afficher les références croisées</translation>
    </message>
    <message>
        <location filename="../menus/AddressableItemContextMenu.cpp" line="19"/>
        <source>Add comment</source>
        <translation>Ajouter un commentaire</translation>
    </message>
</context>
<context>
    <name>AnalClassesModel</name>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="403"/>
        <source>class</source>
        <translation>classe</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="427"/>
        <source>base</source>
        <translation>base</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="458"/>
        <source>method</source>
        <translation>méthode</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="494"/>
        <source>vtable</source>
        <translation>table virtuelle</translation>
    </message>
</context>
<context>
    <name>AnalOptionsWidget</name>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="20"/>
        <source>Analysis</source>
        <translation type="unfinished">Analyse</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="55"/>
        <source>Show verbose information when performing analysis (analysis.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="68"/>
        <source>Analyze push+ret as jmp (analysis.pushret)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="81"/>
        <source>Verbose output from type analysis (analysis.types.verbose)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="94"/>
        <source>Speculatively set a name for the functions (analysis.autoname)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="107"/>
        <source>Search for new functions following already defined functions (analysis.hasnext)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="120"/>
        <source>Create references for unconditional jumps (analysis.jmp.ref)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="133"/>
        <source>Analyze jump tables in switch statements (analysis.jmp.tbl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="161"/>
        <source>Search boundaries for analysis (analysis.in): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="195"/>
        <source>Pointer depth (analysis.ptrdepth):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="236"/>
        <source>Functions Prelude (analysis.prelude):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AnalOptionsWidget.ui" line="269"/>
        <source>Analyze program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnalTask</name>
    <message>
        <location filename="../common/AnalTask.cpp" line="26"/>
        <source>Analyzing Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="43"/>
        <source>Loading the file...</source>
        <translation>Chargement du fichier...</translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="69"/>
        <source>Loading PDB file...</source>
        <translation>Chargement du fichier PDB...</translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="78"/>
        <source>Loading shellcode...</source>
        <translation>Chargement du shellcode...</translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="89"/>
        <source>Executing script...</source>
        <translation>Exécution du script...</translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="98"/>
        <source>Executing analysis...</source>
        <translation>Exécution de l&apos;analyse...</translation>
    </message>
    <message>
        <source>Analyzing...</source>
        <translation type="vanished">Analyse en cours...</translation>
    </message>
    <message>
        <source>Running</source>
        <translation type="vanished">En cours</translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="107"/>
        <source>Analysis complete!</source>
        <translation>Analyse terminée !</translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="109"/>
        <source>Skipping Analysis.</source>
        <translation>Ignorer l&apos;analyse.</translation>
    </message>
    <message>
        <location filename="../common/AnalTask.cpp" line="24"/>
        <source>Initial Analysis</source>
        <translation>Analyse initiale</translation>
    </message>
</context>
<context>
    <name>AppearanceOptionsWidget</name>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="14"/>
        <source>Appearance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="28"/>
        <source>Font:</source>
        <translation>Police:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="56"/>
        <source>Select font</source>
        <translation>Sélectionner la police</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="76"/>
        <source>Zoom</source>
        <translation>Agrandir</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="95"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="116"/>
        <source>Language:</source>
        <translation>Langue :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="133"/>
        <source>Interface Theme:</source>
        <translation>Thème de l&apos;interface :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="160"/>
        <source>Color Theme:</source>
        <translation>Couleur du thème :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="179"/>
        <source>Edit Theme</source>
        <translation>Éditer le thème</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="193"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="241"/>
        <source>Export</source>
        <translation>Exportation</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="255"/>
        <source>Import</source>
        <translation>Importation</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="289"/>
        <source>Use information provided by decompiler when highlighting code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="292"/>
        <source>Decompiler based highlighting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Qt Theme:</source>
        <translation type="vanished">Thème Qt :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="147"/>
        <source>Default</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="152"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <source>Color Theme</source>
        <translation type="vanished">Thème de couleurs</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="213"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="227"/>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Langue&#xa0;</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.ui" line="312"/>
        <source>Save as Default</source>
        <translation>Enregistrer comme valeurs par défaut</translation>
    </message>
    <message>
        <source>Enter scheme name</source>
        <translation type="vanished">Nom du schéma de couleur</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Are you sure you want to delete theme </source>
        <translation type="obsolete">Are you sure you want to delete theme </translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="125"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>Éditeur de thème - &lt;%1&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="136"/>
        <source>Enter theme name</source>
        <translation>Saisissez le nom du thème</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="137"/>
        <source> - copy</source>
        <translation> - copier</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="143"/>
        <source>Theme Copy</source>
        <translation>Copier le thème</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="144"/>
        <source>Theme named %1 already exists.</source>
        <translation>Le nom %1 existe déjà.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="159"/>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="168"/>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="190"/>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="214"/>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="229"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="163"/>
        <source>Are you sure you want to delete &lt;b&gt;%1&lt;/b&gt;?</source>
        <translation>Êtes-vous sûr de vouloir supprimer &lt;b&gt;%1&lt;/b&gt;?</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="185"/>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="211"/>
        <source>Success</source>
        <translation>Terminé avec succès</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="186"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully imported.</source>
        <translation>Le thème de couleur &lt;b&gt;%1&lt;/b&gt; a été importé avec succès.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="212"/>
        <source>Color theme &lt;b&gt;%1&lt;/b&gt; was successfully exported.</source>
        <translation>Le thème de couleur &lt;b&gt;%1&lt;/b&gt; a été exporté avec succès.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="221"/>
        <source>Enter new theme name</source>
        <translation>Saisissez le nom du nouveau thème</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="240"/>
        <source>Language settings</source>
        <translation>Paramètres linguistiques</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="241"/>
        <source>Language will be changed after next application start.</source>
        <translation>La langue sera modifiée après le prochain démarrage de l&apos;application.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AppearanceOptionsWidget.cpp" line="245"/>
        <source>Cannot set language, not found in available ones</source>
        <translation>Impossible de définir la langue, introuvable dans les langues disponibles</translation>
    </message>
</context>
<context>
    <name>AsmOptionsWidget</name>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="14"/>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="65"/>
        <source>Disassembly</source>
        <translation>Désassembleur</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="35"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Show ESIL instead of assembly (asm.esil)</source>
        <translation type="vanished">Afficher ESIL plutôt que de l&apos;assembleur (asm.esil)</translation>
    </message>
    <message>
        <source>Show pseudocode instead of assembly (asm.pseudo)</source>
        <translation type="vanished">Afficher du pseudocode plutôt que de l&apos;assembleur (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="170"/>
        <source>Show offsets (asm.offset)</source>
        <translation>Afficher les adresses (asm.offset)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="71"/>
        <source>Display the bytes of each instruction (asm.bytes)</source>
        <translation>Afficher les octets de chaque instruction (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="291"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="299"/>
        <source>Show opcode description (asm.describe)</source>
        <translation>Afficher la description des opcodes (asm.describe)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="78"/>
        <source>Syntax (asm.syntax):</source>
        <translation>Syntaxe (asm.syntax) :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="198"/>
        <source>Lowercase</source>
        <translation>Minuscules</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="203"/>
        <source>Uppercase (asm.ucase)</source>
        <translation>Majuscules (asm.ucase)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="208"/>
        <source>Capitalize (asm.capitalize)</source>
        <translation>Capitaliser (asm.capitalize)</translation>
    </message>
    <message>
        <source>Separate bytes with whitespace (asm.bytespace)</source>
        <translation type="vanished">Séparer les octets avec des espaces (asm.bytespace)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="121"/>
        <source>Indent disassembly based on reflines depth (asm.indent)</source>
        <translation>Indenter le désassemblage en fonction de la profondeur des reflines (asm.indent)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="98"/>
        <source>Show Disassembly as:</source>
        <translation>Afficher le désassemblage comme:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="190"/>
        <source>Show empty line after every basic block (asm.bb.line)</source>
        <translation>Afficher une ligne vide après chaque basic block (asm.bb.line)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="152"/>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="307"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="157"/>
        <source>ESIL (asm.esil)</source>
        <translation>ESIL (asm.esil)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="162"/>
        <source>Pseudocode (asm.pseudo)</source>
        <translation>Pseudocode (asm.pseudo)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="216"/>
        <source>Align bytes to the left (asm.lbytes)</source>
        <translation>Aligner les octets à gauche (asm.lbytes)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="223"/>
        <source>Separate bytes with whitespace (asm.bytes.space)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="230"/>
        <source>Display flags&apos; real name (asm.flags.real)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="259"/>
        <source>Show offsets relative to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="266"/>
        <source>Functions (asm.reloff)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="275"/>
        <source>Flags (asm.reloff.flags)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="312"/>
        <source>Above instructions</source>
        <translation>Les instructions ci-dessus</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="317"/>
        <source>Off</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="325"/>
        <source>Show comments:</source>
        <translation>Afficher les commentaires:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="475"/>
        <source>Substitute variables (asm.sub.var)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="482"/>
        <source>Substitute entire variable expressions with names (asm.sub.varonly)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="237"/>
        <source>Tabs in assembly (asm.tabs):</source>
        <translation>Tabulations dans le désassemblage (asm.tabs) :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="177"/>
        <source>Tabs before assembly (asm.tabs.off):</source>
        <translation>Tabulations avant l&apos;assembleur (asm.tabs.off) :</translation>
    </message>
    <message>
        <source>Show empty line after every basic block (asm.bbline)</source>
        <translation type="vanished">Afficher une ligne vide après chaque basic block (asm.bbline)</translation>
    </message>
    <message>
        <source>Show comments at right of assembly (asm.cmt.right)</source>
        <translation type="vanished">Afficher les commentaires à droite du désassemblage (asm.cmt.right)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="345"/>
        <source>Column to align comments (asm.cmt.col):</source>
        <translation>Colonne où aligner les commentaires (asm.cmt.col) :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="355"/>
        <source>Show x-refs (asm.xrefs)</source>
        <translation>Afficher les x-refs (asm.xrefs)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="362"/>
        <source>Show refpointer information (asm.refptr)</source>
        <translation>Afficher les informations du refpointeur (asm.refptr)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="395"/>
        <source>Metadata</source>
        <translation>Métadonnée</translation>
    </message>
    <message>
        <source>Show stack pointer (asm.stackptr)</source>
        <translation type="vanished">Afficher le pointeur de pile (asm.stackptr)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="419"/>
        <source>Slow Analysis (asm.slow)</source>
        <translation>Analyse lente (asm.slow)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="426"/>
        <source>Show jump lines (asm.lines)</source>
        <translation>Afficher les lignes de sauts (asm.lines)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="433"/>
        <source>Show function boundary lines (asm.lines.fcn)</source>
        <translation>Afficher les lignes de frontières de fonctions (asm.lines.fcn)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="440"/>
        <source>Show offset before flags (asm.flags.off)</source>
        <translation>Afficher les adresses avant les flags (asm.flags.off)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="447"/>
        <source>Run ESIL emulation analysis (asm.emu)</source>
        <translation>Exécuter une analyse d’émulation ESIL (asm.emu)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="454"/>
        <source>Show only strings if any in the asm.emu output (emu.str)</source>
        <translation>Afficher uniquement les chaînes de caractères si elles sont dans la sortie asm.emu (emu.str)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="461"/>
        <source>Show size of opcodes in disassembly (asm.size)</source>
        <translation>Voir la taille des opcodes au désassemblage (asm.size)</translation>
    </message>
    <message>
        <source>Show bytes (asm.bytes)</source>
        <translation type="vanished">Afficher les octets (asm.bytes)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="468"/>
        <source>Show variables summary instead of full list (asm.var.summary)</source>
        <translation>Afficher les variables sommaires au lieu de la liste la plus complète (asm.var.summary)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="111"/>
        <source>Number of bytes to display (asm.nbytes):</source>
        <translation>Nombre d’octets à afficher (asm.nbytes)&#xa0;:</translation>
    </message>
    <message>
        <source>Substitute variables (asm.var.sub)</source>
        <translation type="vanished">Variables de remplacement (asm.var.sub)</translation>
    </message>
    <message>
        <source>Substitute entire variable expressions with names (asm.var.subonly)</source>
        <translation type="vanished">Substituer les expressions variables entières avec des noms (asm.var.subonly)</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/AsmOptionsWidget.ui" line="538"/>
        <source>Save as Default</source>
        <translation>Enregistrer comme valeurs par défaut</translation>
    </message>
</context>
<context>
    <name>AsyncTaskDialog</name>
    <message>
        <location filename="../dialogs/AsyncTaskDialog.ui" line="14"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../dialogs/AsyncTaskDialog.ui" line="20"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../dialogs/AsyncTaskDialog.cpp" line="43"/>
        <source>Running for</source>
        <translation>Actif depuis</translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/AsyncTaskDialog.cpp" line="45"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation>
            <numerusform>%n heure</numerusform>
            <numerusform>%n heure(s)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/AsyncTaskDialog.cpp" line="49"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minute</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/AsyncTaskDialog.cpp" line="52"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation>
            <numerusform>%n seconde(s)</numerusform>
            <numerusform>%n seconde(s)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>AttachProcDialog</name>
    <message>
        <location filename="../dialogs/AttachProcDialog.ui" line="14"/>
        <source>Select process to attach...</source>
        <translation>Sélectionner le processus à attacher...</translation>
    </message>
    <message>
        <location filename="../dialogs/AttachProcDialog.ui" line="46"/>
        <source>Processes with same name as currently open file:</source>
        <translation>Processus avec le même nom que le fichier actuellement ouvert&#xa0;:</translation>
    </message>
    <message>
        <location filename="../dialogs/AttachProcDialog.ui" line="94"/>
        <source>All processes:</source>
        <translation>Tous les processus&#xa0;:</translation>
    </message>
    <message>
        <location filename="../dialogs/AttachProcDialog.ui" line="138"/>
        <source>Quick Filter</source>
        <translation>Filtre rapide</translation>
    </message>
</context>
<context>
    <name>BacktraceWidget</name>
    <message>
        <source>Func Name</source>
        <translation type="vanished">Nom de fonction</translation>
    </message>
    <message>
        <location filename="../widgets/BacktraceWidget.cpp" line="16"/>
        <source>Function</source>
        <translation>Fonction</translation>
    </message>
    <message>
        <location filename="../widgets/BacktraceWidget.cpp" line="19"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../widgets/BacktraceWidget.cpp" line="20"/>
        <source>Frame Size</source>
        <translation>Taille du cadre</translation>
    </message>
</context>
<context>
    <name>Base64EnDecodedWriteDialog</name>
    <message>
        <location filename="../dialogs/Base64EnDecodedWriteDialog.ui" line="14"/>
        <source>Base64 Encode/Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/Base64EnDecodedWriteDialog.ui" line="24"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/Base64EnDecodedWriteDialog.ui" line="38"/>
        <source>Decode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/Base64EnDecodedWriteDialog.ui" line="48"/>
        <source>Encode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BinClassesModel</name>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="121"/>
        <source>method</source>
        <translation>méthode</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="145"/>
        <source>field</source>
        <translation>champ</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="167"/>
        <source>base class</source>
        <translation>classe de base</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="187"/>
        <source>class</source>
        <translation>classe</translation>
    </message>
</context>
<context>
    <name>BreakpointModel</name>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="64"/>
        <source>HW %1</source>
        <translation>HW %1</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="66"/>
        <source>SW</source>
        <translation>SW</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="101"/>
        <source>Offset</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="103"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="105"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="109"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="111"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Autorisations</translation>
    </message>
    <message>
        <source>Hardware bp</source>
        <translation type="vanished">Point d&apos;arrêt matériel</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="107"/>
        <source>Tracing</source>
        <translation>Tracage</translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="vanished">Actif</translation>
    </message>
</context>
<context>
    <name>BreakpointWidget</name>
    <message>
        <location filename="../widgets/BreakpointWidget.ui" line="58"/>
        <source>Add new breakpoint</source>
        <translation>Ajouter un nouveau point d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.ui" line="65"/>
        <location filename="../widgets/BreakpointWidget.cpp" line="196"/>
        <source>Delete breakpoint</source>
        <translation>Supprimer le point d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.ui" line="72"/>
        <source>Delete all breakpoints</source>
        <translation>Supprimer tous les points d’arrêt</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="202"/>
        <source>Toggle breakpoint</source>
        <translation>Activer/désactiver le point d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../widgets/BreakpointWidget.cpp" line="208"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
</context>
<context>
    <name>BreakpointsDialog</name>
    <message>
        <source>Add breakpoints</source>
        <translation type="vanished">Ajouter des points d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="14"/>
        <source>Add/Edit breakpoint</source>
        <translation>Ajouter/Modifier un point d’arrêt</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="22"/>
        <source>Position</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="59"/>
        <source>Condition</source>
        <translation>État</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="91"/>
        <source>?v $.rax-0x6  # break when rax is 6</source>
        <translation>?v $.rax-0x6 # pause quand rax est à 6</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="99"/>
        <source>Module</source>
        <translation>Modules</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="126"/>
        <source>Type/Options</source>
        <translation>Type/Options</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="132"/>
        <source>Enabled</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="142"/>
        <source>Software</source>
        <translation>Logiciel</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="152"/>
        <source>Hardware</source>
        <translation>Matériel</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="180"/>
        <source>Read</source>
        <translation>Lire</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="187"/>
        <source>Write</source>
        <translation>Écrire</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="194"/>
        <source>Execute</source>
        <translation>Exécuter</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="206"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="217"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="222"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="227"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="232"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="261"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="267"/>
        <source>Trace</source>
        <translation>Tracer</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.ui" line="276"/>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="21"/>
        <source>Edit breakpoint</source>
        <translation>Modifier le point d’arrêt</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="23"/>
        <source>New breakpoint</source>
        <translation>Nouveau point d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="32"/>
        <source>Address or expression calculated when creating breakpoint</source>
        <translation>Adresse ou expression calculée lors de la création d&apos;un point d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Named</source>
        <translation>Nommé</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="34"/>
        <source>Expression - stored as expression</source>
        <translation>Expression - enregistrée en tant qu&apos;expression</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Module offset</source>
        <translation>Décalage du module</translation>
    </message>
    <message>
        <location filename="../dialogs/BreakpointsDialog.cpp" line="35"/>
        <source>Offset relative to module</source>
        <translation>Décalage par rapport au module</translation>
    </message>
</context>
<context>
    <name>CallGraphWidget</name>
    <message>
        <location filename="../widgets/CallGraph.cpp" line="23"/>
        <source>Global Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CallGraph.cpp" line="23"/>
        <source>Callgraph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClassesModel</name>
    <message>
        <source>method</source>
        <translation type="vanished">méthode</translation>
    </message>
    <message>
        <source>field</source>
        <translation type="vanished">champ</translation>
    </message>
    <message>
        <source>class</source>
        <translation type="vanished">classe</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="19"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="21"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="23"/>
        <source>Offset</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="25"/>
        <source>VTable</source>
        <translation>Table virtuelle</translation>
    </message>
</context>
<context>
    <name>ClassesWidget</name>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="79"/>
        <source>Source:</source>
        <translation>Source :</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="93"/>
        <source>Binary Info (Fixed)</source>
        <translation>Infos sur le binaire (Fixé)</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="98"/>
        <source>Analysis (Editable)</source>
        <translation>Analyse (Modifiable)</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="109"/>
        <source>Seek to VTable</source>
        <translation>Chercher vers VTable</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="114"/>
        <source>Edit Method</source>
        <translation>Éditer la Méthode</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="119"/>
        <source>Add Method</source>
        <translation>Ajouter une Méthode</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="124"/>
        <location filename="../widgets/ClassesWidget.cpp" line="743"/>
        <source>Create new Class</source>
        <translation>Créer une nouvelle Classe</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="129"/>
        <source>Rename Class</source>
        <translation>Renommer une Classe</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.ui" line="134"/>
        <location filename="../widgets/ClassesWidget.cpp" line="759"/>
        <source>Delete Class</source>
        <translation>Supprimer la classe</translation>
    </message>
    <message>
        <source>Flags (Editable)</source>
        <translation type="obsolete">Flags (Editable)</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="695"/>
        <source>Missing VTable in class</source>
        <translation>VTable manquant dans la classe</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="696"/>
        <source>The class %1 does not have any VTable!</source>
        <translation>La classe %1 n&apos;a pas de VTable!</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="743"/>
        <source>Class Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="778"/>
        <source>Class name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Class Name</source>
        <translation type="vanished">Nom de la classe</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="760"/>
        <source>Are you sure you want to delete the class %1?</source>
        <translation>Êtes-vous sûrs de vouloir supprimer la class %1?</translation>
    </message>
    <message>
        <location filename="../widgets/ClassesWidget.cpp" line="777"/>
        <source>Rename Class %1</source>
        <translation>Renommer la Classe %1</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="87"/>
        <source>Val:</source>
        <translation>Val:</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="118"/>
        <source>Sat:</source>
        <translation>Sat:</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="149"/>
        <source>Hue:</source>
        <translation>Teinte:</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="184"/>
        <source>Red:</source>
        <translation>Rouge&#xa0;:</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="215"/>
        <source>Green:</source>
        <translation>Vert:</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="246"/>
        <source>Blue:</source>
        <translation>Bleu :</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="281"/>
        <source>Hex:</source>
        <translation>Hex:</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="288"/>
        <source>\#HHHHHH</source>
        <translation>\#HHHHHH</translation>
    </message>
    <message>
        <location filename="../widgets/ColorPicker.ui" line="312"/>
        <source>Pick color from screen</source>
        <translation>Choisir la couleur à partir de l&apos;écran</translation>
    </message>
</context>
<context>
    <name>ColorSchemeFileSaver</name>
    <message>
        <source>Standard themes not found!</source>
        <translation type="vanished">Thèmes standards introuvables&#xa0;!</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found! This probably means radare2 is not properly installed. If you think it is open an issue please.</source>
        <translation type="vanished">Les thèmes standard de radare2 sont introuvables&#xa0;! Cela signifie probablement que radare2 n’est pas correctement installé. Si vous pensez qu&apos;il l&apos;est, merci de nous le signaler.</translation>
    </message>
</context>
<context>
    <name>ColorSchemePrefWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Form</translation>
    </message>
    <message>
        <source>Set Default</source>
        <translation type="vanished">Etablir valeurs par défaut</translation>
    </message>
</context>
<context>
    <name>ColorThemeEditDialog</name>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.ui" line="26"/>
        <source>Color Theme:</source>
        <translation>Couleur du thème :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.ui" line="55"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.cpp" line="35"/>
        <source>Disassembly Preview</source>
        <translation>Aperçu du désassemblage</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.cpp" line="79"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.cpp" line="93"/>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.cpp" line="144"/>
        <source>Unsaved changes</source>
        <translation>Changements non sauvegardés</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.cpp" line="94"/>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.cpp" line="145"/>
        <source>Are you sure you want to exit without saving? All changes will be lost.</source>
        <translation>Êtes-vous sûr de vouloir quitter sans enregistrer ? Toutes les modifications seront perdues.</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/ColorThemeEditDialog.cpp" line="159"/>
        <source>Theme Editor - &lt;%1&gt;</source>
        <translation>Éditeur de thème - &lt;%1&gt;</translation>
    </message>
</context>
<context>
    <name>ColorThemeWorker</name>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="45"/>
        <source>Standard themes not found</source>
        <translation>Thèmes standards introuvables</translation>
    </message>
    <message>
        <source>The radare2 standard themes could not be found. Most likely, radare2 is not properly installed.</source>
        <translation type="vanished">Les thèmes standard radare2 n&apos;ont pas pu être trouvés. Il est très probable que radare2 ne soit pas correctement installé.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="74"/>
        <location filename="../common/ColorThemeWorker.cpp" line="202"/>
        <source>Theme &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>Le thème &lt;b&gt;%1&lt;/b&gt; n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="84"/>
        <source>The file &lt;b&gt;%1&lt;/b&gt; cannot be opened.</source>
        <translation>Le fichier &lt;b&gt;%1&lt;/b&gt; ne peut pas être ouvert.</translation>
    </message>
    <message>
        <source>You can not delete standard radare2 color themes.</source>
        <translation type="vanished">Vous ne pouvez pas supprimer les thèmes de couleur standard de radare2.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="46"/>
        <source>The Rizin standard themes could not be found in &apos;%1&apos;. Most likely, Rizin is not properly installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="199"/>
        <source>You can not delete standard Rizin color themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="207"/>
        <source>You have no permission to write to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Vous n&apos;avez pas la permission d&apos;écrire à &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="210"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be opened.</source>
        <translation>Le fichier &lt;b&gt;%1&lt;/b&gt; ne peut pas être ouvert.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="213"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; can not be removed.</source>
        <translation>Le fichier &lt;b&gt;%1&lt;/b&gt; ne peut pas être supprimé.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="222"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; does not exist.</source>
        <translation>Le fichier &lt;b&gt;%1&lt;/b&gt; n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="228"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; could not be opened. Please make sure you have access to it and try again.</source>
        <translation>Le fichier &lt;b&gt;%1&lt;/b&gt; n&apos;a pas pu être ouvert. Veuillez vous assurer que vous y avez accès et réessayez.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="232"/>
        <source>File &lt;b&gt;%1&lt;/b&gt; is not a Cutter color theme</source>
        <translation>Le fichier &lt;b&gt;%1&lt;/b&gt; n&apos;est pas un thème de couleur Cutter</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="237"/>
        <source>A color theme named &lt;b&gt;%1&lt;/b&gt; already exists.</source>
        <translation>Un thème nommé &lt;b&gt;%1&lt;/b&gt; existe déjà.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="243"/>
        <source>Error occurred during importing. Please make sure you have an access to the directory &lt;b&gt;%1&lt;/b&gt; and try again.</source>
        <translation>Une erreur s&apos;est produite lors de l&apos;importation. Veuillez vous assurer que vous avez un accès au répertoire &lt;b&gt;%1&lt;/b&gt; et réessayez.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="253"/>
        <source>A color theme named &lt;b&gt;&quot;%1&quot;&lt;/b&gt; already exists.</source>
        <translation>Un thème nommé &lt;b&gt;%1&lt;/b&gt; existe déjà.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="257"/>
        <source>You can not rename standard Rizin themes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can not rename standard radare2 themes.</source>
        <translation type="vanished">Vous ne pouvez pas renommer les thèmes standard de radare2.</translation>
    </message>
    <message>
        <location filename="../common/ColorThemeWorker.cpp" line="263"/>
        <source>Something went wrong during renaming. Please make sure you have access to the directory &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</source>
        <translation>Une erreur s&apos;est produite lors du renommage. Veuillez vous assurer que vous avez accès au répertoire &lt;b&gt;&quot;%1&quot;&lt;/b&gt;.</translation>
    </message>
</context>
<context>
    <name>ComboQuickFilterView</name>
    <message>
        <location filename="../widgets/ComboQuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../widgets/ComboQuickFilterView.ui" line="32"/>
        <source>Quick Filter</source>
        <translation>Filtre rapide</translation>
    </message>
    <message>
        <location filename="../widgets/ComboQuickFilterView.ui" line="39"/>
        <source>TextLabel</source>
        <translation>Etiquette</translation>
    </message>
</context>
<context>
    <name>CommandTask</name>
    <message>
        <location filename="../common/CommandTask.h" line="23"/>
        <source>Running Command</source>
        <translation>Éxécution de la commande</translation>
    </message>
</context>
<context>
    <name>CommentsDialog</name>
    <message>
        <location filename="../dialogs/CommentsDialog.ui" line="14"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../dialogs/CommentsDialog.cpp" line="43"/>
        <source>Add Comment at %1</source>
        <translation>Ajouter un commentaire à %1</translation>
    </message>
    <message>
        <location filename="../dialogs/CommentsDialog.cpp" line="45"/>
        <source>Edit Comment at %1</source>
        <translation>Modifier le commentaire à %1</translation>
    </message>
</context>
<context>
    <name>CommentsModel</name>
    <message>
        <location filename="../widgets/CommentsWidget.cpp" line="157"/>
        <source>Function/Offset</source>
        <translation>Fonction/Offset</translation>
    </message>
    <message>
        <location filename="../widgets/CommentsWidget.cpp" line="159"/>
        <location filename="../widgets/CommentsWidget.cpp" line="170"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../widgets/CommentsWidget.cpp" line="166"/>
        <source>Offset</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location filename="../widgets/CommentsWidget.cpp" line="168"/>
        <source>Function</source>
        <translation>Fonction</translation>
    </message>
</context>
<context>
    <name>CommentsWidget</name>
    <message>
        <location filename="../widgets/CommentsWidget.cpp" line="236"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../widgets/CommentsWidget.cpp" line="239"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <source>Horizontal view</source>
        <translation type="vanished">Vue horizontale</translation>
    </message>
    <message>
        <location filename="../widgets/CommentsWidget.cpp" line="237"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
    <message>
        <source>Vertical view</source>
        <translation type="vanished">Vue verticale</translation>
    </message>
</context>
<context>
    <name>Configuration</name>
    <message>
        <location filename="../common/Configuration.cpp" line="138"/>
        <source>Critical!</source>
        <translation>Critical!</translation>
    </message>
    <message>
        <location filename="../common/Configuration.cpp" line="139"/>
        <source>!!! Settings are not writable! Make sure you have a write access to &quot;%1&quot;</source>
        <translation>!!! Les paramètres ne sont pas modifiables ! Assurez-vous d&apos;avoir un accès en écriture à &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>ConsoleWidget</name>
    <message>
        <source>R2 Console</source>
        <translation type="vanished">Console R2</translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.ui" line="93"/>
        <source>Rizin Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.ui" line="98"/>
        <source>Debugee Input</source>
        <translation>Entrée de debug</translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.ui" line="115"/>
        <source> Type &quot;?&quot; for help</source>
        <translation> Tapez &quot;?&quot; pour obtenir de l&apos;aide</translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.ui" line="137"/>
        <source> Enter input for the debugee</source>
        <translation> Arguments pour le débogage</translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.ui" line="153"/>
        <source>Execute command</source>
        <translation>Exécuter une commande</translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.ui" line="159"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.cpp" line="75"/>
        <source>Clear Output</source>
        <translation>Effacer la sortie</translation>
    </message>
    <message>
        <location filename="../widgets/ConsoleWidget.cpp" line="84"/>
        <source>Wrap Lines</source>
        <translation>Retour à la ligne</translation>
    </message>
</context>
<context>
    <name>CutterCore</name>
    <message>
        <location filename="../core/Cutter.cpp" line="1717"/>
        <source>Starting native debug...</source>
        <translation>Démarrage du débogage natif...</translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="1760"/>
        <source>Starting emulation...</source>
        <translation>Démarrage de l&apos;émulation...</translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="1814"/>
        <source>Connecting to: </source>
        <translation>Connexion à : </translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="1853"/>
        <source>Attaching to process (</source>
        <translation>Attacher au processus (</translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="2209"/>
        <source>Creating debug tracepoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="2245"/>
        <source>Stopping debug session...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="2279"/>
        <source>Breakpoint error</source>
        <translation>Erreur de breakpoint</translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="2279"/>
        <source>Failed to create breakpoint</source>
        <translation>Impossible de créer le breakpoint</translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="3144"/>
        <source>Unknown (%1)</source>
        <translation>Inconnu (%1)</translation>
    </message>
    <message>
        <location filename="../core/Cutter.cpp" line="3396"/>
        <source>Primitive</source>
        <translation>Primitive</translation>
    </message>
</context>
<context>
    <name>CutterGraphView</name>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="30"/>
        <location filename="../widgets/CutterGraphView.cpp" line="442"/>
        <source>Export Graph</source>
        <translation type="unfinished">Exporter graphe</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="42"/>
        <source>Layout</source>
        <translation type="unfinished">Calque</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="43"/>
        <source>Horizontal</source>
        <translation type="unfinished">Horizontal</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="47"/>
        <source>Grid narrow</source>
        <translation type="unfinished">Grille petite</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="48"/>
        <source>Grid medium</source>
        <translation type="unfinished">Grille médium</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="49"/>
        <source>Grid wide</source>
        <translation type="unfinished">Grille large</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="63"/>
        <source>Graphviz polyline</source>
        <translation type="unfinished">Visualisation de graphe polyligne</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="64"/>
        <source>Graphviz ortho</source>
        <translation type="unfinished">Visualisation de graphe ortho</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="65"/>
        <source>Graphviz sfdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="66"/>
        <source>Graphviz neato</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="67"/>
        <source>Graphviz twopi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="68"/>
        <source>Graphviz circo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="409"/>
        <source>PNG (*.png)</source>
        <translation type="unfinished">PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="410"/>
        <source>JPEG (*.jpg)</source>
        <translation type="unfinished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="411"/>
        <source>SVG (*.svg)</source>
        <translation type="unfinished">SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="417"/>
        <source>Graphviz dot (*.dot)</source>
        <translation type="unfinished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="418"/>
        <source>Graph Modelling Language (*.gml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="420"/>
        <source>RZ JSON (*.json)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="421"/>
        <source>SDB key-value (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="427"/>
        <source>Graphviz json (*.json)</source>
        <translation type="unfinished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="429"/>
        <source>Graphviz gif (*.gif)</source>
        <translation type="unfinished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="431"/>
        <source>Graphviz png (*.png)</source>
        <translation type="unfinished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="433"/>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="unfinished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="435"/>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="unfinished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="437"/>
        <source>Graphviz svg (*.svg)</source>
        <translation type="unfinished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="460"/>
        <source>Graph Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/CutterGraphView.cpp" line="461"/>
        <source>Do you really want to export %1 x %2 = %3 pixel bitmap image? Consider using different format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterSeekable</name>
    <message>
        <location filename="../widgets/AddressableDockWidget.cpp" line="45"/>
        <source> (unsynced)</source>
        <translation> (non synchronisé)</translation>
    </message>
    <message>
        <location filename="../common/CutterSeekable.cpp" line="73"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CutterTreeWidget</name>
    <message>
        <location filename="../widgets/CutterTreeWidget.cpp" line="19"/>
        <source>%1 Items</source>
        <translation>%1 Objets</translation>
    </message>
</context>
<context>
    <name>Dashboard</name>
    <message>
        <location filename="../widgets/Dashboard.ui" line="116"/>
        <source>OVERVIEW</source>
        <translation>AFFICHAGE</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="140"/>
        <source>Info</source>
        <translation>Infos</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="178"/>
        <source>File:</source>
        <translation>Fichier:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="188"/>
        <location filename="../widgets/Dashboard.ui" line="217"/>
        <location filename="../widgets/Dashboard.ui" line="246"/>
        <location filename="../widgets/Dashboard.ui" line="291"/>
        <location filename="../widgets/Dashboard.ui" line="320"/>
        <location filename="../widgets/Dashboard.ui" line="349"/>
        <location filename="../widgets/Dashboard.ui" line="378"/>
        <location filename="../widgets/Dashboard.ui" line="391"/>
        <location filename="../widgets/Dashboard.ui" line="436"/>
        <location filename="../widgets/Dashboard.ui" line="465"/>
        <location filename="../widgets/Dashboard.ui" line="494"/>
        <location filename="../widgets/Dashboard.ui" line="523"/>
        <location filename="../widgets/Dashboard.ui" line="552"/>
        <location filename="../widgets/Dashboard.ui" line="581"/>
        <location filename="../widgets/Dashboard.ui" line="610"/>
        <location filename="../widgets/Dashboard.ui" line="655"/>
        <location filename="../widgets/Dashboard.ui" line="668"/>
        <location filename="../widgets/Dashboard.ui" line="710"/>
        <location filename="../widgets/Dashboard.ui" line="739"/>
        <location filename="../widgets/Dashboard.ui" line="768"/>
        <location filename="../widgets/Dashboard.ui" line="797"/>
        <location filename="../widgets/Dashboard.ui" line="826"/>
        <location filename="../widgets/Dashboard.ui" line="855"/>
        <location filename="../widgets/Dashboard.ui" line="884"/>
        <location filename="../widgets/Dashboard.ui" line="913"/>
        <location filename="../widgets/Dashboard.ui" line="942"/>
        <source>--</source>
        <translation>--</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="207"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="236"/>
        <source>Mode:</source>
        <translation>Mode :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="265"/>
        <source>Size:</source>
        <translation>Taille :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="281"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="310"/>
        <source>Class:</source>
        <translation>Classe :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="339"/>
        <source>Language:</source>
        <translation>Langue&#xa0;:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="368"/>
        <source>Bits:</source>
        <translation>Bits :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="426"/>
        <source>FD:</source>
        <translation>Descripteur de Fichier :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="455"/>
        <source>Base addr:</source>
        <translation>Addresse de base :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="484"/>
        <source>Virtual addr:</source>
        <translation>Adresse virtuelle :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="513"/>
        <source>Canary:</source>
        <translation>Canary:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="542"/>
        <source>Crypto:</source>
        <translation>Crypto:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="571"/>
        <source>NX bit:</source>
        <translation>Bit NX :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="600"/>
        <source>PIC:</source>
        <translation>PIC:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="629"/>
        <source>Static:</source>
        <translation>Statistique :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="645"/>
        <source>Relro:</source>
        <translation>Relro:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="700"/>
        <source>Architecture:</source>
        <translation>Architecture:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="729"/>
        <source>Machine:</source>
        <translation>Machine :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="758"/>
        <source>OS:</source>
        <translation>OS:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="787"/>
        <source>Subsystem:</source>
        <translation>Sous-système:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="816"/>
        <source>Stripped:</source>
        <translation>Strippé:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="845"/>
        <source>Relocs:</source>
        <translation>Relocs:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="874"/>
        <source>Endianness:</source>
        <translation>Boutisme:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="903"/>
        <source>Compiled:</source>
        <translation>Compilé:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="932"/>
        <source>Compiler:</source>
        <translation>Compilateur :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="993"/>
        <source>Certificates</source>
        <translation>Certificats</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1006"/>
        <source>Version info</source>
        <translation>Information de version</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1034"/>
        <source>Hashes</source>
        <translation>Hashes</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1051"/>
        <source>Libraries</source>
        <translation>Bibliothèques</translation>
    </message>
    <message>
        <source>MD5:</source>
        <translation type="vanished">MD5:</translation>
    </message>
    <message>
        <source>SHA1:</source>
        <translation type="vanished">SHA1&#xa0;:</translation>
    </message>
    <message>
        <source>Entropy:</source>
        <translation type="vanished">Entropie&#xa0;:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1083"/>
        <source>Analysis info</source>
        <translation>Infos sur l&apos;analyse</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1107"/>
        <source>Functions:</source>
        <translation>Fonctions :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1133"/>
        <source>X-Refs:</source>
        <translation>Références croisées :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1159"/>
        <source>Calls:</source>
        <translation>Appels :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1185"/>
        <source>Strings:</source>
        <translation>Chaînes de caractères :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1211"/>
        <source>Symbols:</source>
        <translation>Symboles :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1237"/>
        <source>Imports:</source>
        <translation>Importations :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1263"/>
        <source>Analysis coverage:</source>
        <translation>Couverture d&apos;analyse:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1289"/>
        <source>Code size:</source>
        <translation>Taille du code :</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.ui" line="1315"/>
        <source>Coverage percent:</source>
        <translation>Pourcentage de couverture:</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.cpp" line="124"/>
        <source>&lt;b&gt;Entropy:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.cpp" line="236"/>
        <location filename="../widgets/Dashboard.cpp" line="256"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.cpp" line="251"/>
        <source>True</source>
        <translation>True</translation>
    </message>
    <message>
        <location filename="../widgets/Dashboard.cpp" line="253"/>
        <source>False</source>
        <translation>Faux</translation>
    </message>
</context>
<context>
    <name>DebugActions</name>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="56"/>
        <source>Start debug</source>
        <translation>Démarrer le débogage</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="37"/>
        <source>Start emulation</source>
        <translation>Démarrer l’émulation</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="38"/>
        <source>Attach to process</source>
        <translation>Attacher au processus</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="40"/>
        <source>Stop debug</source>
        <translation>Arrêter le débogage</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="41"/>
        <source>Stop emulation</source>
        <translation>Arrêter l’émulation</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="55"/>
        <source>Restart program</source>
        <translation>Redémarrer le programme</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="42"/>
        <source>Restart emulation</source>
        <translation>Redémarrer l&apos;émulation</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="54"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="43"/>
        <source>Continue until main</source>
        <translation>Continuer jusqu’à la fonction principale</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="39"/>
        <source>Connect to a remote debugger</source>
        <translation>Se connecter à un débogueur distant</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="44"/>
        <source>Continue until call</source>
        <translation>Continuer jusqu&apos;à l&apos;appel</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="45"/>
        <source>Continue until syscall</source>
        <translation>Continuer jusqu&apos;au syscall</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="46"/>
        <source>Continue backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="47"/>
        <source>Step</source>
        <translation>Étape</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="48"/>
        <source>Step over</source>
        <translation>Sauter</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="49"/>
        <source>Step out</source>
        <translation>Sortir</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="50"/>
        <source>Step backwards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="51"/>
        <source>Start trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="52"/>
        <source>Stop trace session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="53"/>
        <source>Suspend the process</source>
        <translation>Suspendre le processus</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="147"/>
        <source>Debugged process exited (</source>
        <translation>Processus débogué quitté (</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="267"/>
        <source>Debug is currently in beta.
</source>
        <translation>Le débogage est actuellement en version bêta.
</translation>
    </message>
    <message>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/radareorg/cutter/issues</source>
        <translation type="vanished">Si vous rencontrez des problèmes ou avez des suggestions, veuillez les soumettre à l&apos;adresse suivante : https://github.com/radareorg/cutter/issues</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="295"/>
        <source>Error connecting.</source>
        <translation>Erreur de connexion.</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="377"/>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation>Le fichier &apos;%1&apos; n&apos;est pas executable.</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="348"/>
        <source>Error attaching. No process selected!</source>
        <translation>Erreur lors de l&apos;attachement. Aucun processus sélectionné!</translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="268"/>
        <source>If you encounter any problems or have suggestions, please submit an issue to https://github.com/rizinorg/cutter/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DebugActions.cpp" line="282"/>
        <location filename="../widgets/DebugActions.cpp" line="357"/>
        <source>Detach from process</source>
        <translation>Se détacher de processus</translation>
    </message>
</context>
<context>
    <name>DebugOptionsWidget</name>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="14"/>
        <source>Debug</source>
        <translation>Débogage</translation>
    </message>
    <message>
        <source>Debug Plugin:</source>
        <translation type="vanished">Plugin de Débogage :</translation>
    </message>
    <message>
        <source>Program Arguments:</source>
        <translation type="vanished">Arguments de programme&#xa0;:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="30"/>
        <source>Debug plugin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="42"/>
        <source>ESIL options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="69"/>
        <source>ESIL stack address:</source>
        <translation>Adresse de la pile ESIL :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="79"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="82"/>
        <source>ESIL stack size:</source>
        <translation>Taille de la pile ESIL :</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="100"/>
        <source>Trace options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="109"/>
        <source>Trace each step during continue in a trace session (dbg.trace_continue)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="112"/>
        <source>Disabling this option means that stepping back after continue will return to the previous PC. Significantly improves performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/DebugOptionsWidget.ui" line="54"/>
        <source>Break esil execution when instruction is invalid (esil.breakoninvalid)</source>
        <translation>Stopper l&apos;exécution ESIL lorsqu&apos;une instruction est invalide (asm.breakoninvalid)</translation>
    </message>
</context>
<context>
    <name>DebugToolbar</name>
    <message>
        <source>Start debug</source>
        <translation type="vanished">Démarrer le débogage</translation>
    </message>
    <message>
        <source>Start emulation</source>
        <translation type="vanished">Démarrer l’émulation</translation>
    </message>
    <message>
        <source>Attach to process</source>
        <translation type="vanished">Attacher au processus</translation>
    </message>
    <message>
        <source>Stop debug</source>
        <translation type="vanished">Arrêter le débogage</translation>
    </message>
    <message>
        <source>Stop emulation</source>
        <translation type="vanished">Arrêter l’émulation</translation>
    </message>
    <message>
        <source>Restart program</source>
        <translation type="vanished">Redémarrer le programme</translation>
    </message>
    <message>
        <source>Restart emulation</source>
        <translation type="vanished">Redémarrer l’émulation</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Continuer</translation>
    </message>
    <message>
        <source>Continue until main</source>
        <translation type="vanished">Continuer jusqu’à la fonction principale</translation>
    </message>
    <message>
        <source>Continue until call</source>
        <translation type="vanished">Continuer jusqu&apos;à un appel</translation>
    </message>
    <message>
        <source>Continue until syscall</source>
        <translation type="vanished">Continuer jusqu&apos;à un appel système</translation>
    </message>
    <message>
        <source>Step</source>
        <translation type="vanished">Pas</translation>
    </message>
    <message>
        <source>Step over</source>
        <translation type="vanished">Sauter</translation>
    </message>
    <message>
        <source>Step out</source>
        <translation type="vanished">Sortir</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; does not have executable permissions.</source>
        <translation type="vanished">Le fichier &apos;%1&apos; n&apos;est pas exécutable.</translation>
    </message>
    <message>
        <source>Error attaching. No process selected!</source>
        <translation type="vanished">Erreur lors de l&apos;attachement. Aucun processus sélectionné !</translation>
    </message>
    <message>
        <source>Detach from process</source>
        <translation type="vanished">Se détacher de processus</translation>
    </message>
</context>
<context>
    <name>DecompilerContextMenu</name>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="26"/>
        <source>Copy</source>
        <translation type="unfinished">Copier</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="27"/>
        <source>Copy instruction address (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="28"/>
        <source>Copy address of [flag] (&lt;address&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="29"/>
        <source>Show in</source>
        <translation type="unfinished">Afficher dans</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="30"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="161"/>
        <source>Add Comment</source>
        <translation type="unfinished">Ajouter commentaire</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="31"/>
        <source>Delete comment</source>
        <translation type="unfinished">Supprimer le commentaire</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="32"/>
        <source>Rename function at cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="33"/>
        <source>Delete &lt;name&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="34"/>
        <source>Edit variable &lt;name of variable&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="35"/>
        <source>Show X-Refs</source>
        <translation type="unfinished">Afficher les références croisées</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="36"/>
        <source>Add/remove breakpoint</source>
        <translation type="unfinished">Ajouter/Supprimer point d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="37"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="192"/>
        <source>Advanced breakpoint</source>
        <translation type="unfinished">Breakpoint avancé</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="39"/>
        <source>Continue until line</source>
        <translation type="unfinished">Continuer jusqu&apos;à la ligne</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="40"/>
        <source>Set PC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="164"/>
        <source>Edit Comment</source>
        <translation type="unfinished">Modifier le commentaire</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="180"/>
        <source>Add breakpoint</source>
        <translation type="unfinished">Ajouter un breakpoint</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="182"/>
        <source>Remove breakpoint</source>
        <translation type="unfinished">Supprimer le point d’arrêt</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="184"/>
        <source>Remove all breakpoints in line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="191"/>
        <source>Edit breakpoint</source>
        <translation type="unfinished">Modifier le point d’arrêt</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="195"/>
        <source>Set %1 here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="207"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Renommer la fonction %1</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="212"/>
        <source>Rename %1</source>
        <translation type="unfinished">Renommer %1</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="213"/>
        <source>Remove %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="216"/>
        <source>Add name to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="221"/>
        <source>Copy instruction address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="227"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="232"/>
        <source>Copy address of %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="236"/>
        <source>Copy address (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="252"/>
        <source>Edit variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="254"/>
        <source>Rename variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="316"/>
        <source>Can&apos;t rename this variable.&lt;br&gt;Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="335"/>
        <source>Can&apos;t edit this variable.&lt;br&gt;Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="407"/>
        <source>Define this function at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="408"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="415"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="414"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Renommer la fonction %2</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="424"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="449"/>
        <source>Rename %2</source>
        <translation type="unfinished">Renommer %2</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="425"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="432"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="450"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="432"/>
        <source>Add name to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="443"/>
        <source>Rename local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="444"/>
        <source>Can&apos;t rename this variable. Only local variables defined in disassembly can be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="468"/>
        <source>Edit local variable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="469"/>
        <source>Can&apos;t edit this variable. Only local variables defined in disassembly can be edited.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="535"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="545"/>
        <source>Debug</source>
        <translation type="unfinished">Débogage</translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="575"/>
        <location filename="../menus/DecompilerContextMenu.cpp" line="577"/>
        <source>Show %1 in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DecompilerContextMenu.cpp" line="582"/>
        <source>%1 (%2)</source>
        <translation type="unfinished">%1 (%2)</translation>
    </message>
</context>
<context>
    <name>DecompilerWidget</name>
    <message>
        <location filename="../widgets/DecompilerWidget.ui" line="14"/>
        <location filename="../widgets/DecompilerWidget.cpp" line="452"/>
        <source>Decompiler</source>
        <translation>Décompileur</translation>
    </message>
    <message>
        <source>Auto Refresh</source>
        <translation type="vanished">Rafraîchissement Automatique</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Rafraîchir</translation>
    </message>
    <message>
        <location filename="../widgets/DecompilerWidget.ui" line="53"/>
        <source>Decompiling...</source>
        <translation>Décompilation...</translation>
    </message>
    <message>
        <location filename="../widgets/DecompilerWidget.ui" line="75"/>
        <source>Decompiler:</source>
        <translation>Décompileur:</translation>
    </message>
    <message>
        <location filename="../widgets/DecompilerWidget.cpp" line="33"/>
        <source>Choose an offset and refresh to get decompiled code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DecompilerWidget.cpp" line="69"/>
        <source>No Decompiler available.</source>
        <translation>Aucun décompilateur disponible.</translation>
    </message>
    <message>
        <location filename="../widgets/DecompilerWidget.cpp" line="262"/>
        <source>No function found at this offset. Seek to a function or define one in order to decompile it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Click Refresh to generate Decompiler from current offset.</source>
        <translation type="vanished">Cliquez sur Rafraîchir pour générer le décompilateur à partir du décalage actuel.</translation>
    </message>
    <message>
        <location filename="../widgets/DecompilerWidget.cpp" line="309"/>
        <source>Cannot decompile at this address (Not a function?)</source>
        <translation>Impossible de décompiler à cette adresse (N&apos;est pas une fonction?)</translation>
    </message>
</context>
<context>
    <name>DisassemblerGraphView</name>
    <message>
        <source>Export Graph</source>
        <translation type="vanished">Exporter graphe</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Synchroniser/Désynchroniser l&apos;adresse</translation>
    </message>
    <message>
        <source>Grid narrow</source>
        <translation type="vanished">Grille petite</translation>
    </message>
    <message>
        <source>Grid medium</source>
        <translation type="vanished">Grille médium</translation>
    </message>
    <message>
        <source>Grid wide</source>
        <translation type="vanished">Grille large</translation>
    </message>
    <message>
        <source>Graphviz polyline</source>
        <translation type="vanished">Visualisation de graphe polyligne</translation>
    </message>
    <message>
        <source>Graphviz polyline LR</source>
        <translation type="vanished">Visualisation de graphe polyligne LR</translation>
    </message>
    <message>
        <source>Graphviz ortho</source>
        <translation type="vanished">Visualisation de graphe ortho</translation>
    </message>
    <message>
        <source>Graphviz ortho LR</source>
        <translation type="vanished">Visualisation de graphe ortho LR</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation type="vanished">Calque</translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblerGraphView.cpp" line="91"/>
        <source>Highlight block</source>
        <translation>Bloc de surbrillance</translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblerGraphView.cpp" line="109"/>
        <source>Unhighlight block</source>
        <translation>Annuler la mise en surbrillance du bloc</translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblerGraphView.cpp" line="119"/>
        <source>Highlight instruction</source>
        <translation>Surligner les instructions</translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblerGraphView.cpp" line="123"/>
        <source>Unhighlight instruction</source>
        <translation>Instructions pour annuler la surbrillance</translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblerGraphView.cpp" line="199"/>
        <source>No function detected. Cannot display graph.</source>
        <translation>Aucune fonction détectée. Impossible d&apos;affiche un graphe.</translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblerGraphView.cpp" line="214"/>
        <source>Graph</source>
        <translation>Graphe</translation>
    </message>
    <message>
        <source>Graphviz dot (*.dot)</source>
        <translation type="vanished">Graphviz dot (*.dot)</translation>
    </message>
    <message>
        <source>Graphviz json (*.json)</source>
        <translation type="vanished">Graphviz json (*.json)</translation>
    </message>
    <message>
        <source>Graphviz gif (*.gif)</source>
        <translation type="vanished">Graphviz gif (*.gif)</translation>
    </message>
    <message>
        <source>Graphviz png (*.png)</source>
        <translation type="vanished">Graphviz png (*.png)</translation>
    </message>
    <message>
        <source>Graphviz jpg (*.jpg)</source>
        <translation type="vanished">Graphviz jpg (*.jpg)</translation>
    </message>
    <message>
        <source>Graphviz PostScript (*.ps)</source>
        <translation type="vanished">Graphviz PostScript (*.ps)</translation>
    </message>
    <message>
        <source>Graphviz svg (*.svg)</source>
        <translation type="vanished">Graphviz svg (*.svg)</translation>
    </message>
    <message>
        <source>Graphiz dot (*.dot)</source>
        <translation type="vanished">Graphiz dot (*.dot)</translation>
    </message>
    <message>
        <source>GIF (*.gif)</source>
        <translation type="vanished">GIF (*.gif)</translation>
    </message>
    <message>
        <source>PNG (*.png)</source>
        <translation type="vanished">PNG (*.png)</translation>
    </message>
    <message>
        <source>JPEG (*.jpg)</source>
        <translation type="vanished">JPEG (*.jpg)</translation>
    </message>
    <message>
        <source>PostScript (*.ps)</source>
        <translation type="vanished">PostScript (*.ps)</translation>
    </message>
    <message>
        <source>SVG (*.svg)</source>
        <translation type="vanished">SVG (*.svg)</translation>
    </message>
    <message>
        <source>JSON (*.json)</source>
        <translation type="vanished">JSON (*.json)</translation>
    </message>
</context>
<context>
    <name>DisassemblyContextMenu</name>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="72"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="75"/>
        <source>Copy address</source>
        <translation>Copier l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="79"/>
        <source>Show in</source>
        <translation>Afficher dans</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="84"/>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="549"/>
        <source>Add Comment</source>
        <translation>Ajouter commentaire</translation>
    </message>
    <message>
        <source>Add Flag</source>
        <translation type="vanished">Ajouter Flag</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">Renommer</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="96"/>
        <source>Edit function</source>
        <translation>Modifier la fonction</translation>
    </message>
    <message>
        <source>Rename Flag/Fcn/Var Used Here</source>
        <translation type="vanished">Renommer Flag/Fcn/Var utilisé ici</translation>
    </message>
    <message>
        <source>Re-type function local vars</source>
        <translation type="vanished">Redéfinir les variables locales</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="100"/>
        <source>Delete comment</source>
        <translation>Supprimer le commentaire</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="104"/>
        <source>Delete flag</source>
        <translation>Supprimer flag</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="107"/>
        <source>Undefine function</source>
        <translation>Annuler la définition de fonction</translation>
    </message>
    <message>
        <source>Define function here...</source>
        <translation type="vanished">Définir la fonction ici...</translation>
    </message>
    <message>
        <source>Set to Code</source>
        <translation type="vanished">Définir comme Code</translation>
    </message>
    <message>
        <source>Set as Code</source>
        <translation type="obsolete">Set as Code</translation>
    </message>
    <message>
        <source>Set as String</source>
        <translation type="obsolete">Set as String</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="133"/>
        <source>Show X-Refs</source>
        <translation>Afficher les références croisées</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="137"/>
        <source>X-Refs for local variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="141"/>
        <source>Show Options</source>
        <translation>Afficher les options</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="172"/>
        <source>Set Immediate Base to...</source>
        <translation>Définir la base immédiate à...</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="174"/>
        <source>Binary</source>
        <translation>Binaire</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="178"/>
        <source>Octal</source>
        <translation>Octal</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="182"/>
        <source>Decimal</source>
        <translation>Décimal</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="186"/>
        <source>Hexadecimal</source>
        <translation>Hexadécimal</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="190"/>
        <source>Network Port</source>
        <translation>Port réseau</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="194"/>
        <source>IP Address</source>
        <translation>Adresse&#xa0;IP</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="198"/>
        <source>Syscall</source>
        <translation>Appel système</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="202"/>
        <source>String</source>
        <translation>Chaîne</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="209"/>
        <source>Set current bits to...</source>
        <translation>Définir les bits acuels à...</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="424"/>
        <source>Rename local &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="429"/>
        <source>Rename flag &quot;%1&quot; (used here)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="826"/>
        <source>New function at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="827"/>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="841"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="840"/>
        <source>Rename function %2</source>
        <translation type="unfinished">Renommer la fonction %2</translation>
    </message>
    <message>
        <source>Set to Data...</source>
        <translation type="vanished">Définir comme Données...</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="111"/>
        <source>Define function here</source>
        <translation>Définir la fonction ici</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="121"/>
        <source>Structure offset</source>
        <translation>Offset de structure</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="125"/>
        <source>Link Type to Address</source>
        <translation>Type de lien vers l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="226"/>
        <source>Set as...</source>
        <translation>Définir comme...</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="228"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="232"/>
        <source>String...</source>
        <translation>Chaîne de caractères...</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="234"/>
        <source>Auto-detect</source>
        <translation>Détection automatique</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="236"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Adanced</source>
        <translation type="vanished">Avancé</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="250"/>
        <source>Data...</source>
        <translation>Données...</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="252"/>
        <source>Byte</source>
        <translation>Octet</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="256"/>
        <source>Word</source>
        <translation>Mot</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="260"/>
        <source>Dword</source>
        <translation>Dword</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="264"/>
        <source>Qword</source>
        <translation>Qword</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="279"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="281"/>
        <source>Instruction</source>
        <translation>Instruction</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="285"/>
        <source>Nop Instruction</source>
        <translation>Remplacer par NOP</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="289"/>
        <source>Bytes</source>
        <translation>Octets</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="292"/>
        <source>Reverse Jump</source>
        <translation>Saut Inversé</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="298"/>
        <source>Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="310"/>
        <source>Debug</source>
        <translation>Débogage</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="300"/>
        <source>Add/remove breakpoint</source>
        <translation>Ajouter/Supprimer point d&apos;arrêt</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="88"/>
        <source>Rename or add flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="92"/>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="875"/>
        <source>Re-type Local Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="238"/>
        <source>Advanced</source>
        <translation type="unfinished">Avancé</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="303"/>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="587"/>
        <source>Advanced breakpoint</source>
        <translation>Breakpoint avancé</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="312"/>
        <source>Continue until line</source>
        <translation>Continuer jusqu&apos;à la ligne</translation>
    </message>
    <message>
        <source>%1 (used here)</source>
        <translation type="vanished">%1 (utilisé ici)</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation type="vanished">%1 (%2)</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="552"/>
        <source>Edit Comment</source>
        <translation>Modifier le commentaire</translation>
    </message>
    <message>
        <source>Rename function &quot;%1&quot;</source>
        <translation type="vanished">Renommez la fonction &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename flag &quot;%1&quot;</source>
        <translation type="vanished">Rename flag &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="568"/>
        <source>Edit function &quot;%1&quot;</source>
        <translation>Modifier la fonction &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="414"/>
        <source>Add flag at %1 (used here)</source>
        <translation>Ajouter un drapeau à %1 (utilisé ici)</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="419"/>
        <source>Rename &quot;%1&quot;</source>
        <translation>Renommer &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Rename &quot;%1&quot; (used here)</source>
        <translation type="vanished">Renommer &quot;%1&quot; (utilisé ici)</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Remove breakpoint</source>
        <translation>Supprimer le point d’arrêt</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="585"/>
        <source>Add breakpoint</source>
        <translation>Ajouter un breakpoint</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="586"/>
        <source>Edit breakpoint</source>
        <translation>Modifier le point d’arrêt</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="601"/>
        <source>X-Refs for %1</source>
        <translation type="unfinished">Références croisées pour %1</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="701"/>
        <source>Edit Instruction at %1</source>
        <translation>Modifier l&apos;instruction à %1</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="755"/>
        <source>Edit Bytes at %1</source>
        <translation>Modifier les octets à %1</translation>
    </message>
    <message>
        <source>Write error</source>
        <translation type="vanished">Erreur d&apos;écriture</translation>
    </message>
    <message>
        <source>Unable to complete write operation. Consider opening in write mode. 

WARNING: In write mode any changes will be commited to disk</source>
        <translation type="vanished">Impossible de terminer l&apos;opération d&apos;écriture. Ouvre en mode d&apos;écriture. 

ATTENTION : En mode écriture, toutes les modifications seront validées sur le disque</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Reopen in write mode and try again</source>
        <translation type="vanished">Rouvrir en mode écriture et réessayer</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="938"/>
        <source>Wrong address</source>
        <translation>Mauvaise adresse</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="939"/>
        <source>Can&apos;t edit string at this address</source>
        <translation>Impossible de modifier la chaîne à cette adresse</translation>
    </message>
    <message>
        <source>Add Comment at %1</source>
        <translation type="obsolete">Add Comment at %1</translation>
    </message>
    <message>
        <source>Edit Comment at %1</source>
        <translation type="obsolete">Edit Comment at %1</translation>
    </message>
    <message>
        <source>Analyze function at %1</source>
        <translation type="vanished">Analyser la fonction à %1</translation>
    </message>
    <message>
        <source>Function name</source>
        <translation type="vanished">Nom de la fonction</translation>
    </message>
    <message>
        <source>Rename function %1</source>
        <translation type="vanished">Renommer la fonction %1</translation>
    </message>
    <message>
        <source>Rename flag %1</source>
        <translation type="vanished">Renommer le flag %1</translation>
    </message>
    <message>
        <source>Add flag at %1</source>
        <translation type="vanished">Ajouter flag à %1</translation>
    </message>
    <message>
        <source>Rename %1</source>
        <translation type="vanished">Renommer %1</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="876"/>
        <source>You must be in a function to define variable types.</source>
        <translation>Vous devez être dans une fonction pour définir les types de variables.</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="obsolete">Set Variable Types for Function: %1</translation>
    </message>
    <message>
        <location filename="../menus/DisassemblyContextMenu.cpp" line="1020"/>
        <source>Edit function %1</source>
        <translation>Modifier la fonction %1</translation>
    </message>
</context>
<context>
    <name>DisassemblyWidget</name>
    <message>
        <location filename="../widgets/DisassemblyWidget.cpp" line="670"/>
        <source>More than one (%1) references here. Weird behaviour expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblyWidget.cpp" line="676"/>
        <source>offsetFrom (%1) differs from refs.at(0).from (%(2))</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DisassemblyWidget.cpp" line="721"/>
        <source>Disassembly</source>
        <translation>Vue Désassembleur</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Sync/désync le décalage</translation>
    </message>
</context>
<context>
    <name>DuplicateFromOffsetDialog</name>
    <message>
        <location filename="../dialogs/DuplicateFromOffsetDialog.ui" line="20"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/DuplicateFromOffsetDialog.ui" line="28"/>
        <source>Offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/DuplicateFromOffsetDialog.ui" line="49"/>
        <source>N bytes:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditFunctionDialog</name>
    <message>
        <location filename="../dialogs/EditFunctionDialog.ui" line="14"/>
        <source>Edit Function</source>
        <translation>Modifier la fonction</translation>
    </message>
    <message>
        <location filename="../dialogs/EditFunctionDialog.ui" line="43"/>
        <source>Name of function</source>
        <translation>Nom de la fonction</translation>
    </message>
    <message>
        <location filename="../dialogs/EditFunctionDialog.ui" line="53"/>
        <source>Start address</source>
        <translation>Adresse de départ</translation>
    </message>
    <message>
        <source>End address</source>
        <translation type="vanished">Adresse de fin</translation>
    </message>
    <message>
        <location filename="../dialogs/EditFunctionDialog.ui" line="63"/>
        <source>Stack size</source>
        <translation>Taille de la pile</translation>
    </message>
    <message>
        <location filename="../dialogs/EditFunctionDialog.ui" line="73"/>
        <source>Calling convention</source>
        <translation>Convention d’appel</translation>
    </message>
</context>
<context>
    <name>EditInstructionDialog</name>
    <message>
        <location filename="../dialogs/EditInstructionDialog.ui" line="14"/>
        <source>Edit Instruction</source>
        <translation>Modifier Instruction</translation>
    </message>
    <message>
        <location filename="../dialogs/EditInstructionDialog.ui" line="76"/>
        <source>Unknown Instruction</source>
        <translation>Instruction inconnue</translation>
    </message>
</context>
<context>
    <name>EditMethodDialog</name>
    <message>
        <location filename="../dialogs/EditMethodDialog.ui" line="25"/>
        <source>Class:</source>
        <translation>Classe:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditMethodDialog.ui" line="32"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditMethodDialog.ui" line="42"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditMethodDialog.ui" line="52"/>
        <source>Virtual:</source>
        <translation>Virtuel:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditMethodDialog.ui" line="66"/>
        <source>Offset in VTable:</source>
        <translation>Offset dans la table virtuelle:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditMethodDialog.cpp" line="152"/>
        <source>Create Method</source>
        <translation>Créer une méthode</translation>
    </message>
    <message>
        <location filename="../dialogs/EditMethodDialog.cpp" line="167"/>
        <source>Edit Method</source>
        <translation>Modifier la méthode</translation>
    </message>
</context>
<context>
    <name>EditStringDialog</name>
    <message>
        <location filename="../dialogs/EditStringDialog.ui" line="23"/>
        <source>Edit string</source>
        <translation>Modifier chaîne</translation>
    </message>
    <message>
        <location filename="../dialogs/EditStringDialog.ui" line="66"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditStringDialog.ui" line="52"/>
        <source>Size:</source>
        <translation>Taille:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditStringDialog.ui" line="59"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../dialogs/EditStringDialog.ui" line="99"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>EditVariablesDialog</name>
    <message>
        <location filename="../dialogs/EditVariablesDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../dialogs/EditVariablesDialog.ui" line="22"/>
        <source>Modify:</source>
        <translation>Modifier:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditVariablesDialog.ui" line="35"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../dialogs/EditVariablesDialog.ui" line="42"/>
        <source>Type:</source>
        <translation>Type:</translation>
    </message>
    <message>
        <source>Set Variable Types for Function: %1</source>
        <translation type="vanished">Définir les types de variables pour la fonction : %1</translation>
    </message>
    <message>
        <location filename="../dialogs/EditVariablesDialog.cpp" line="19"/>
        <source>Edit Variables in Function: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportsModel</name>
    <message>
        <location filename="../widgets/ExportsWidget.cpp" line="60"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../widgets/ExportsWidget.cpp" line="62"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/ExportsWidget.cpp" line="64"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/ExportsWidget.cpp" line="66"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/ExportsWidget.cpp" line="68"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>ExportsWidget</name>
    <message>
        <location filename="../widgets/ExportsWidget.cpp" line="138"/>
        <source>Exports</source>
        <translation>Exports</translation>
    </message>
</context>
<context>
    <name>FlagDialog</name>
    <message>
        <location filename="../dialogs/FlagDialog.ui" line="14"/>
        <source>Add Flag</source>
        <translation>Ajouter Flag</translation>
    </message>
    <message>
        <location filename="../dialogs/FlagDialog.ui" line="20"/>
        <source>Add flag at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/FlagDialog.ui" line="38"/>
        <source>Flag:</source>
        <translation>Flag:</translation>
    </message>
    <message>
        <location filename="../dialogs/FlagDialog.ui" line="61"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../dialogs/FlagDialog.ui" line="80"/>
        <source>Size:</source>
        <translation>Taille:</translation>
    </message>
    <message>
        <location filename="../dialogs/FlagDialog.cpp" line="24"/>
        <source>Edit flag at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/FlagDialog.cpp" line="26"/>
        <source>Add flag at %1</source>
        <translation type="unfinished">Ajouter flag à %1</translation>
    </message>
</context>
<context>
    <name>FlagsModel</name>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="64"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="70"/>
        <source>Real Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="72"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>FlagsWidget</name>
    <message>
        <location filename="../widgets/FlagsWidget.ui" line="79"/>
        <source>Quick Filter</source>
        <translation>Filtre rapide</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.ui" line="86"/>
        <source>Flagspace:</source>
        <translation>Flagspace:</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.ui" line="99"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.ui" line="102"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.ui" line="110"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.ui" line="113"/>
        <source>Del</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="214"/>
        <source>Rename flag %1</source>
        <translation type="unfinished">Renommer le flag %1</translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="215"/>
        <source>Flag name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/FlagsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(tous)</translation>
    </message>
</context>
<context>
    <name>FunctionModel</name>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="123"/>
        <source>Offset: %1</source>
        <translation>Offset : %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="125"/>
        <source>Size: %1</source>
        <translation>Taille: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="127"/>
        <source>Import: %1</source>
        <translation>Importer: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="128"/>
        <source>true</source>
        <translation>true</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="128"/>
        <source>false</source>
        <translation>false</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="130"/>
        <source>Nargs: %1</source>
        <translation>Nargs: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="132"/>
        <source>Nbbs: %1</source>
        <translation>Nbbs : %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="134"/>
        <source>Nlocals: %1</source>
        <translation>Nlocals: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="281"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
    <message>
        <source>Cyclomatic complexity: %1</source>
        <translation type="obsolete">Cyclomatic complexity: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="136"/>
        <source>Call type: %1</source>
        <translation>Type d&apos;appel: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="138"/>
        <source>Edges: %1</source>
        <translation>Bordures : %1</translation>
    </message>
    <message>
        <source>Cost: %1</source>
        <translation type="obsolete">Cost: %1</translation>
    </message>
    <message>
        <source>Calls/OutDegree: %1</source>
        <translation type="obsolete">Calls/OutDegree: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="140"/>
        <source>StackFrame: %1</source>
        <translation>StackFrame: %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="142"/>
        <source>Comment: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="221"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Aperçu du dédassemblage&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="226"/>
        <source>&lt;div&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div&gt;&lt;strong&gt;Mis en avant&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div&gt;&lt;strong&gt;Summary&lt;/strong&gt;:&lt;br&gt;</source>
        <translation type="obsolete">&lt;div&gt;&lt;strong&gt;Summary&lt;/strong&gt;:&lt;br&gt;</translation>
    </message>
    <message>
        <source>Size:&amp;nbsp;%1,&amp;nbsp;Cyclomatic complexity:&amp;nbsp;%2,&amp;nbsp;Basic blocks:&amp;nbsp;%3</source>
        <translation type="obsolete">Size:&amp;nbsp;%1,&amp;nbsp;Cyclomatic complexity:&amp;nbsp;%2,&amp;nbsp;Basic blocks:&amp;nbsp;%3</translation>
    </message>
    <message>
        <source>&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="obsolete">&lt;/div&gt;&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Disassembly preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <source>&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation type="obsolete">&lt;div style=&quot;margin-top: 10px;&quot;&gt;&lt;strong&gt;Highlights&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="257"/>
        <location filename="../widgets/FunctionsWidget.cpp" line="261"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="263"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="265"/>
        <source>Imp.</source>
        <translation>Imp.</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="267"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="269"/>
        <source>Nargs</source>
        <translation>Nargs</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="273"/>
        <source>Nbbs</source>
        <translation>Nbbs</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="271"/>
        <source>Nlocals</source>
        <translation>Nlocals</translation>
    </message>
    <message>
        <source>Cyclo. Comp.</source>
        <translation type="vanished">Complexité Cyclomatique</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="275"/>
        <source>Call type</source>
        <translation>Type d&apos;appel</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="277"/>
        <source>Edges</source>
        <translation>Bords</translation>
    </message>
    <message>
        <source>Cost</source>
        <translation type="vanished">Coût</translation>
    </message>
    <message>
        <source>Calls/OutDeg.</source>
        <translation type="obsolete">Calls/OutDeg.</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="279"/>
        <source>StackFrame</source>
        <translation>StackFrame</translation>
    </message>
</context>
<context>
    <name>FunctionsTask</name>
    <message>
        <location filename="../common/FunctionsTask.h" line="13"/>
        <source>Fetching Functions</source>
        <translation>Récupération des fonctions</translation>
    </message>
</context>
<context>
    <name>FunctionsWidget</name>
    <message>
        <source>Add comment</source>
        <translation type="vanished">Ajouter un commentaire</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="442"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="443"/>
        <source>Undefine</source>
        <translation>Indéfini</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="447"/>
        <source>Functions</source>
        <translation>Fonctions</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="549"/>
        <source>Rename function %1</source>
        <translation type="unfinished">Renommer la fonction %1</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="550"/>
        <source>Function name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X-Refs</source>
        <translation type="vanished">Références croisées</translation>
    </message>
    <message>
        <source>Cross references</source>
        <translation type="vanished">Références croisées</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="444"/>
        <source>Horizontal</source>
        <translation>Horizontal</translation>
    </message>
    <message>
        <location filename="../widgets/FunctionsWidget.cpp" line="445"/>
        <source>Vertical</source>
        <translation>Vertical</translation>
    </message>
</context>
<context>
    <name>GraphOptionsWidget</name>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="14"/>
        <source>Graph</source>
        <translation>Graph</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="22"/>
        <source>Graph Block Options </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="31"/>
        <source>The offset of the first instruction of a graph block is shown in the header of the respective graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="34"/>
        <source>Show offset of the first instruction in each graph block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="41"/>
        <source>Show offset for each instruction (graph.offset)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="50"/>
        <source>Maximum Line Length:</source>
        <translation>Longueur maximale de la ligne:</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="70"/>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="80"/>
        <source>Hide text when zooming out and it is smaller than the given value. Higher values can increase Performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="73"/>
        <source>Minimum Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="98"/>
        <source>Graph Layout Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="117"/>
        <source>Vertical</source>
        <translation type="unfinished">Vertical</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="140"/>
        <source>Horizontal</source>
        <translation type="unfinished">Horizontal</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="176"/>
        <source>Block spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="183"/>
        <source>Edge spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="193"/>
        <source>Bitmap Export Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="202"/>
        <source>Export Transparent Bitmap Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="211"/>
        <source>Graph Bitmap Export Scale: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/GraphOptionsWidget.ui" line="218"/>
        <source>%</source>
        <translation type="unfinished">%</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset)</source>
        <translation type="vanished">Afficher les offsets (graph.offset)</translation>
    </message>
    <message>
        <source>Show offsets (graph.offset) </source>
        <translation type="obsolete">Show offsets (graph.offset) </translation>
    </message>
</context>
<context>
    <name>HeadersModel</name>
    <message>
        <location filename="../widgets/HeadersWidget.cpp" line="55"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../widgets/HeadersWidget.cpp" line="57"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/HeadersWidget.cpp" line="59"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../widgets/HeadersWidget.cpp" line="61"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>HeadersWidget</name>
    <message>
        <location filename="../widgets/HeadersWidget.cpp" line="120"/>
        <source>Headers</source>
        <translation>Entêtes</translation>
    </message>
</context>
<context>
    <name>HexWidget</name>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="67"/>
        <source>Hexadecimal</source>
        <translation>Hexadécimal</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="68"/>
        <source>Octal</source>
        <translation>Octal</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="69"/>
        <source>Decimal</source>
        <translation>Décimal</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="70"/>
        <source>Signed decimal</source>
        <translation>Décimale signée</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="71"/>
        <source>Float</source>
        <translation>Flottant</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="85"/>
        <source>Bytes per row</source>
        <translation>Octets par ligne</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="95"/>
        <source>Power of 2</source>
        <translation>Puissance de 2</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="102"/>
        <source>Big Endian</source>
        <translation>Big Endian</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="107"/>
        <source>Bytes as pairs</source>
        <translation>Octets par pairs</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="111"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="117"/>
        <source>Copy address</source>
        <translation>Copier l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="123"/>
        <source>Select range</source>
        <translation>Sélectionner une portée</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="130"/>
        <location filename="../widgets/HexWidget.cpp" line="705"/>
        <source>Write string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="134"/>
        <source>Write length and string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="138"/>
        <location filename="../widgets/HexWidget.cpp" line="842"/>
        <source>Write wide string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="142"/>
        <source>Write zero terminated string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="146"/>
        <source>Write De\Encoded Base64 string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="151"/>
        <location filename="../widgets/HexWidget.cpp" line="745"/>
        <source>Write zeros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="155"/>
        <source>Write random bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="159"/>
        <source>Duplicate from offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="163"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="620"/>
        <source>Item size:</source>
        <translation>Taille de l&apos;élément:</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="622"/>
        <source>Item format:</source>
        <translation>Format de l&apos;élément:</translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="627"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="705"/>
        <location filename="../widgets/HexWidget.cpp" line="826"/>
        <location filename="../widgets/HexWidget.cpp" line="842"/>
        <location filename="../widgets/HexWidget.cpp" line="857"/>
        <source>String:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="745"/>
        <source>Number of zeros:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="769"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="770"/>
        <source>Error occured during decoding your input.
Please, make sure, that it is a valid base64 string and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="793"/>
        <source>Write random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="793"/>
        <source>Number of bytes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="826"/>
        <source>Write Pascal string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexWidget.cpp" line="857"/>
        <source>Write zero-terminated string</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HexdumpRangeDialog</name>
    <message>
        <location filename="../dialogs/HexdumpRangeDialog.ui" line="14"/>
        <source>Select Block</source>
        <translation>Sélectionner le bloc</translation>
    </message>
    <message>
        <location filename="../dialogs/HexdumpRangeDialog.ui" line="47"/>
        <location filename="../dialogs/HexdumpRangeDialog.ui" line="79"/>
        <source>Exclusive end address</source>
        <translation>Adresse de fin exclusive</translation>
    </message>
    <message>
        <location filename="../dialogs/HexdumpRangeDialog.ui" line="50"/>
        <source>End Address:</source>
        <translation>Adresse de fin:</translation>
    </message>
    <message>
        <location filename="../dialogs/HexdumpRangeDialog.ui" line="65"/>
        <source>Start Address:</source>
        <translation>Adresse de début:</translation>
    </message>
    <message>
        <location filename="../dialogs/HexdumpRangeDialog.ui" line="99"/>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <location filename="../dialogs/HexdumpRangeDialog.ui" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Big selection might cause a delay&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ff8585;&quot;&gt;Une grande sélection peut entraîner un délai&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>HexdumpWidget</name>
    <message>
        <source>0  1  2  3 ...</source>
        <translation type="vanished">0  1  2  3 ...</translation>
    </message>
    <message>
        <source>0123...</source>
        <translation type="vanished">0123...</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation type="vanished">Offset</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="70"/>
        <source>Parsing</source>
        <translation>Analyse en cours</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="50"/>
        <source>Select bytes to display information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="143"/>
        <source>Disassembly</source>
        <translation>Vue Désassembleur</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="144"/>
        <source>String</source>
        <translation>Chaîne</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="145"/>
        <source>Assembler</source>
        <translation>Assembleur</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="146"/>
        <source>C bytes</source>
        <translation>Octets C</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="147"/>
        <source>C bytes with instructions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="148"/>
        <source>C half-words (2 byte)</source>
        <translation>Demi-mots C (2 octets)</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="149"/>
        <source>C words (4 byte)</source>
        <translation>Mots C (4 octets)</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="150"/>
        <source>C dwords (8 byte)</source>
        <translation>Double-mots C (8 octets)</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="151"/>
        <source>Python</source>
        <translation>Python</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="152"/>
        <source>JSON</source>
        <translation>JSON</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="153"/>
        <source>JavaScript</source>
        <translation>JavaScript</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="154"/>
        <source>Yara</source>
        <translation>Yara</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="120"/>
        <source>Endian</source>
        <translation>Endian</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="131"/>
        <source>Little</source>
        <translation>Petit</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="136"/>
        <source>Big</source>
        <translation>Grand</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="191"/>
        <source>Arch</source>
        <translation>Architecture</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="220"/>
        <source>Bits</source>
        <translation>Bits</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="231"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="236"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="241"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="318"/>
        <source>SHA256:</source>
        <translation type="unfinished">SHA1: {256:?}</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="357"/>
        <source>Copy SHA256</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="396"/>
        <source>Copy CRC32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="466"/>
        <source>MD5:</source>
        <translation>MD5:</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="479"/>
        <source>CRC32:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="350"/>
        <source>SHA1:</source>
        <translation>SHA1:</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.ui" line="434"/>
        <source>Entropy:</source>
        <translation>Entropie&#xa0;:</translation>
    </message>
    <message>
        <source>Hexdump side panel</source>
        <translation type="obsolete">Hexdump side panel</translation>
    </message>
    <message>
        <source>Undefine</source>
        <translation type="obsolete">Undefine</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">Tout copier</translation>
    </message>
    <message>
        <source>Copy bytes</source>
        <translation type="vanished">Copier les octets</translation>
    </message>
    <message>
        <source>Copy disasm</source>
        <translation type="obsolete">Copy disasm</translation>
    </message>
    <message>
        <source>Copy Hexpair</source>
        <translation type="obsolete">Copy Hexpair</translation>
    </message>
    <message>
        <source>Copy ASCII</source>
        <translation type="obsolete">Copy ASCII</translation>
    </message>
    <message>
        <source>Copy Text</source>
        <translation type="obsolete">Copy Text</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="vanished">8</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Modifier</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Paste</translation>
    </message>
    <message>
        <source>Insert Hex</source>
        <translation type="obsolete">Insert Hex</translation>
    </message>
    <message>
        <source>Insert String</source>
        <translation type="obsolete">Insert String</translation>
    </message>
    <message>
        <source>Hex</source>
        <translation type="obsolete">Hex</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="vanished">Octal</translation>
    </message>
    <message>
        <source>Half-word</source>
        <translation type="obsolete">Half-word</translation>
    </message>
    <message>
        <source>Word</source>
        <translation type="obsolete">Word</translation>
    </message>
    <message>
        <source>Quad-word</source>
        <translation type="obsolete">Quad-word</translation>
    </message>
    <message>
        <source>Emoji</source>
        <translation type="obsolete">Emoji</translation>
    </message>
    <message>
        <source>1 byte</source>
        <translation type="obsolete">1 byte</translation>
    </message>
    <message>
        <source>2 bytes</source>
        <translation type="obsolete">2 bytes</translation>
    </message>
    <message>
        <source>4 bytes</source>
        <translation type="obsolete">4 bytes</translation>
    </message>
    <message>
        <source>Select Block...</source>
        <translation type="obsolete">Select Block...</translation>
    </message>
    <message>
        <location filename="../widgets/HexdumpWidget.cpp" line="219"/>
        <source>Hexdump</source>
        <translation>Hexdump</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="obsolete">Columns</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">Format</translation>
    </message>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Synchroniser/désynchroniser l&apos;offset</translation>
    </message>
    <message>
        <source>Error: Could not select range, end address is less then start address</source>
        <translation type="obsolete">Error: Could not select range, end address is less then start address</translation>
    </message>
</context>
<context>
    <name>ImportsModel</name>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="45"/>
        <source>Unsafe</source>
        <translation>Non sûr</translation>
    </message>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="71"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="73"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="75"/>
        <source>Safety</source>
        <translation>Sécurité</translation>
    </message>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="77"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="79"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="81"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>ImportsWidget</name>
    <message>
        <location filename="../widgets/ImportsWidget.cpp" line="172"/>
        <source>Imports</source>
        <translation>Imports</translation>
    </message>
</context>
<context>
    <name>IncrementDecrementDialog</name>
    <message>
        <location filename="../dialogs/IncrementDecrementDialog.ui" line="14"/>
        <source>Increment/Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/IncrementDecrementDialog.ui" line="24"/>
        <source>Interpret as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/IncrementDecrementDialog.ui" line="38"/>
        <source>Value:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/IncrementDecrementDialog.ui" line="54"/>
        <source>Increment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/IncrementDecrementDialog.ui" line="64"/>
        <source>Decrement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Byte</source>
        <translation type="unfinished">Octet</translation>
    </message>
    <message>
        <location filename="../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Dword</source>
        <translation type="unfinished">Dword</translation>
    </message>
    <message>
        <location filename="../dialogs/WriteCommandsDialogs.cpp" line="33"/>
        <source>Qword</source>
        <translation type="unfinished">Qword</translation>
    </message>
</context>
<context>
    <name>InitialOptionsDialog</name>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="26"/>
        <source>Load Options</source>
        <translation>Options de chargement</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="84"/>
        <source>Program:</source>
        <translation>Programme:</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="161"/>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="363"/>
        <source>Analysis: Enabled</source>
        <translation>Analyse : Activée</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="177"/>
        <source>Level: </source>
        <translation>Niveau: </translation>
    </message>
    <message>
        <source>Analyze all symbols (aa)</source>
        <translation type="obsolete">Analyze all symbols (aa)</translation>
    </message>
    <message>
        <source>Analyze for references (aar)</source>
        <translation type="obsolete">Analyze for references (aar)</translation>
    </message>
    <message>
        <source>Analyze function calls (aac)</source>
        <translation type="obsolete">Analyze function calls (aac)</translation>
    </message>
    <message>
        <source>Analyze all basic blocks (aab)</source>
        <translation type="obsolete">Analyze all basic blocks (aab)</translation>
    </message>
    <message>
        <source>Autorename functions based on context (aan)</source>
        <translation type="obsolete">Autorename functions based on context (aan)</translation>
    </message>
    <message>
        <source>Experimental:</source>
        <translation type="obsolete">Experimental:</translation>
    </message>
    <message>
        <source>Emulate code to find computed references (aae)</source>
        <translation type="obsolete">Emulate code to find computed references (aae)</translation>
    </message>
    <message>
        <source>Analyze for consecutive function (aat)</source>
        <translation type="obsolete">Analyze for consecutive function (aat)</translation>
    </message>
    <message>
        <source>Type and Argument matching analysis (afta)</source>
        <translation type="obsolete">Type and Argument matching analysis (afta)</translation>
    </message>
    <message>
        <source>Analyze code after trap-sleds (aaT)</source>
        <translation type="obsolete">Analyze code after trap-sleds (aaT)</translation>
    </message>
    <message>
        <source>Analyze function preludes (aap)</source>
        <translation type="obsolete">Analyze function preludes (aap)</translation>
    </message>
    <message>
        <source>Analyze jump tables in switch statements (e! anal.jmptbl)</source>
        <translation type="obsolete">Analyze jump tables in switch statements (e! anal.jmptbl)</translation>
    </message>
    <message>
        <source>Analyze push+ret as jmp (e! anal.pushret)</source>
        <translation type="obsolete">Analyze push+ret as jmp (e! anal.pushret)</translation>
    </message>
    <message>
        <source>Continue analysis after each function (e! anal.hasnext)</source>
        <translation type="obsolete">Continue analysis after each function (e! anal.hasnext)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="322"/>
        <source>Load in write mode (-w)</source>
        <translation>Charger en mode écriture (-w)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="332"/>
        <source>Do not load bin information (-n)</source>
        <translation>Ne pas charger les informations du binaire (-n)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="345"/>
        <source>Use virtual addressing</source>
        <translation>Utiliser l&apos;adressage virtuelle</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="355"/>
        <source>Import demangled symbols</source>
        <translation>Importer des symboles demandés</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="372"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="403"/>
        <source>Advanced options</source>
        <translation>Options avancées</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="433"/>
        <source>CPU options</source>
        <translation>Options du Processeur</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="448"/>
        <source>Architecture:</source>
        <translation>Architecture:</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="459"/>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="519"/>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="567"/>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="612"/>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="637"/>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="37"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="473"/>
        <source>CPU:</source>
        <translation>CPU:</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="524"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="529"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="534"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="539"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="553"/>
        <source>Endianness: </source>
        <translation>Boutisme: </translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="572"/>
        <source>Little</source>
        <translation>Petit</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="577"/>
        <source>Big</source>
        <translation>Grand</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="598"/>
        <source>Kernel: </source>
        <translation>Noyau: </translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="626"/>
        <source>Format:</source>
        <translation>Format:</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="680"/>
        <source>Load bin offset (-B)</source>
        <translation>Charger les offsets du binaire (-B)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="699"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="718"/>
        <source>Map offset (-m)</source>
        <translation>Tracer l&apos;offset (-m)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="734"/>
        <source>0x40000</source>
        <translation>0x40000</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="741"/>
        <source>Load PDB</source>
        <translation>Charger PDB</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="766"/>
        <source>PDB File path</source>
        <translation>Chemin du fichier PDB</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="773"/>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="815"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="783"/>
        <source>Load script file</source>
        <translation>Charger le fichier de script</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="808"/>
        <source>Path to Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to radare2 script file</source>
        <translation type="vanished">Chemin vers le fichier de script radare2</translation>
    </message>
    <message>
        <source>BasicBlock maxsize:</source>
        <translation type="obsolete">BasicBlock maxsize:</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="871"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.ui" line="884"/>
        <source>  Ok  </source>
        <translation>  Ok  </translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="55"/>
        <source>Analyze all symbols</source>
        <translation>Analyser tous les symboles</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="56"/>
        <source>Analyze instructions for references</source>
        <translation>Analyser les instructions pour les références</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="57"/>
        <source>Analyze function calls</source>
        <translation>Analyser les appels de fonction</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="58"/>
        <source>Analyze all basic blocks</source>
        <translation>Analyser tous les basic blocks</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="59"/>
        <source>Analyze all objc references</source>
        <translation>Analyser toutes les références des objets</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="60"/>
        <source>Recover class information from RTTI</source>
        <translation>Récupérer les informations de la classe à partir de RTTI</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="61"/>
        <source>Autoname functions based on context</source>
        <translation>Fonctions nommées automatiquement grâce au contexte</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="62"/>
        <source>Emulate code to find computed references</source>
        <translation>Émuler le code pour trouver les références calculées</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="63"/>
        <source>Analyze all consecutive functions</source>
        <translation>Analyser toutes les fonctions consécutives</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="64"/>
        <source>Type and Argument matching analysis</source>
        <translation>Analyse des types et Arguments</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="65"/>
        <source>Analyze code after trap-sleds</source>
        <translation>Analyser le code après les trap-sleds</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="66"/>
        <source>Analyze function preludes</source>
        <translation>Analyser les préludes de fonction</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="67"/>
        <source>Analyze jump tables in switch statements</source>
        <translation>Analyser les tables de saut dans les instructions de commutation</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="70"/>
        <source>Analyze PUSH+RET as JMP</source>
        <translation>Analyser PUSH+RET en temps que JMP</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="71"/>
        <source>Continue analysis after each function</source>
        <translation>Continuer l&apos;analyse après chaque fonction</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="343"/>
        <source>No analysis</source>
        <translation>Pas d&apos;analyse</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="345"/>
        <source>Auto-Analysis (aaa)</source>
        <translation>Analyse automatique (aaa)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="347"/>
        <source>Auto-Analysis Experimental (aaaa)</source>
        <translation>Auto-analyse expérimentale (aaaa)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="349"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="351"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="357"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="360"/>
        <source>Analysis: Disabled</source>
        <translation>Analyse : Désactivée</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="406"/>
        <source>Select PDB file</source>
        <translation>Sélectionner un fichier PDB</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <source>PDB file (*.pdb)</source>
        <translation>Fichier PDB (*.pdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="407"/>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>All files (*)</source>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="428"/>
        <source>Select Rizin script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/InitialOptionsDialog.cpp" line="429"/>
        <source>Script file (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select radare2 script file</source>
        <translation type="vanished">Sélectionnez le fichier de script radare2</translation>
    </message>
    <message>
        <source>Script file (*.r2)</source>
        <translation type="vanished">Fichier de script (*.r2)</translation>
    </message>
</context>
<context>
    <name>InitializationFileEditor</name>
    <message>
        <location filename="../dialogs/preferences/InitializationFileEditor.ui" line="14"/>
        <source>CutterRC Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/InitializationFileEditor.ui" line="20"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/InitializationFileEditor.cpp" line="35"/>
        <source>Script is loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JSDecDecompiler</name>
    <message>
        <location filename="../common/Decompiler.cpp" line="40"/>
        <source>Failed to parse JSON from jsdec</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JupyterWebView</name>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
</context>
<context>
    <name>JupyterWidget</name>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <source>Cutter has been built without QtWebEngine.&lt;br /&gt;Open the following URL in your Browser to use Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation type="obsolete">Cutter has been built without QtWebEngine.&lt;br /&gt;Open the following URL in your Browser to use Jupyter:&lt;br /&gt;&lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <source>An error occurred while opening jupyter. Make sure Jupyter is installed system-wide.</source>
        <translation type="obsolete">An error occurred while opening jupyter. Make sure Jupyter is installed system-wide.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../dialogs/LayoutManager.ui" line="14"/>
        <source>Layout</source>
        <translation type="unfinished">Calque</translation>
    </message>
    <message>
        <location filename="../dialogs/LayoutManager.ui" line="25"/>
        <source>Rename</source>
        <translation type="unfinished">Renommer</translation>
    </message>
    <message>
        <location filename="../dialogs/LayoutManager.ui" line="32"/>
        <location filename="../dialogs/LayoutManager.cpp" line="60"/>
        <source>Delete</source>
        <translation type="unfinished">Supprimer</translation>
    </message>
    <message>
        <location filename="../dialogs/LayoutManager.cpp" line="42"/>
        <source>Rename layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/LayoutManager.cpp" line="43"/>
        <source>&apos;%1&apos; is already used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/LayoutManager.cpp" line="45"/>
        <source>Save layout</source>
        <translation type="unfinished">Sauvegarder la disposition</translation>
    </message>
    <message>
        <location filename="../dialogs/LayoutManager.cpp" line="45"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/LayoutManager.cpp" line="61"/>
        <source>Do you want to delete &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LinkTypeDialog</name>
    <message>
        <location filename="../dialogs/LinkTypeDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.ui" line="26"/>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.ui" line="43"/>
        <source>Enter Address</source>
        <translation>Saisir une adresse</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.ui" line="50"/>
        <source>Structure Type</source>
        <translation>Type de structure</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.ui" line="73"/>
        <source>Address/Flag</source>
        <translation>Adresse/Flag</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.cpp" line="10"/>
        <source>Link type to address</source>
        <translation>Type de lien vers l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.cpp" line="13"/>
        <location filename="../dialogs/LinkTypeDialog.cpp" line="57"/>
        <source>(No Type)</source>
        <translation>(Aucun Type)</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.cpp" line="75"/>
        <source>The given address is invalid</source>
        <translation>L&apos;adresse donnée est invalide</translation>
    </message>
    <message>
        <location filename="../dialogs/LinkTypeDialog.cpp" line="106"/>
        <source>Invalid Address</source>
        <translation>Adresse invalide</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../core/MainWindow.ui" line="33"/>
        <source>Add extra...</source>
        <translation>Ajouter une option...</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="61"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="68"/>
        <source>Set mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="94"/>
        <location filename="../core/MainWindow.cpp" line="288"/>
        <source>View</source>
        <translation>View</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="101"/>
        <source>Zoom</source>
        <translation>Agrandir</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="110"/>
        <source>Layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="130"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="138"/>
        <location filename="../core/MainWindow.ui" line="498"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="147"/>
        <source>Windows</source>
        <translation>Fenêtres</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="151"/>
        <location filename="../core/MainWindow.cpp" line="142"/>
        <location filename="../core/MainWindow.cpp" line="143"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="156"/>
        <source>Info...</source>
        <translation>Infos...</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="161"/>
        <source>Debug...</source>
        <translation>Débogage en cours...</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="176"/>
        <source>Debug</source>
        <translation>Débogage</translation>
    </message>
    <message>
        <source>Reset Layout</source>
        <translation type="vanished">Réinitialiser la disposition</translation>
    </message>
    <message>
        <source>Reset layout</source>
        <translation type="vanished">Réinitialiser la disposition</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="227"/>
        <source>Zen Mode</source>
        <translation>Mode Zen</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="230"/>
        <source>Zen mode</source>
        <translation>Mode Zen</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="235"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="240"/>
        <source>Report an issue</source>
        <translation>Signaler un problème</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Nouveau</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="248"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="253"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="258"/>
        <location filename="../core/MainWindow.ui" line="261"/>
        <location filename="../core/MainWindow.ui" line="847"/>
        <location filename="../core/MainWindow.cpp" line="1207"/>
        <source>Save layout</source>
        <translation>Sauvegarder la disposition</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="266"/>
        <source>Documentation</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Ouvrir</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="vanished">Ctrl+O</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Sauvegarder</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="282"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="340"/>
        <source>Undo Seek</source>
        <translation>Annuler la recherche</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="352"/>
        <source>Redo Seek</source>
        <translation>Refaire la recherche</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="287"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="222"/>
        <source>Reset to default layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="245"/>
        <source>New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="271"/>
        <source>Map File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="274"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="279"/>
        <location filename="../core/MainWindow.cpp" line="712"/>
        <location filename="../core/MainWindow.cpp" line="735"/>
        <source>Save Project</source>
        <translation type="unfinished">Sauvegarder le projet</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="292"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="297"/>
        <location filename="../core/MainWindow.ui" line="503"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="302"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="307"/>
        <location filename="../core/MainWindow.ui" line="310"/>
        <source>Select all</source>
        <translation>Tout sélectionner</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="315"/>
        <source>Find</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="320"/>
        <location filename="../core/MainWindow.ui" line="323"/>
        <source>Find next</source>
        <translation>Trouver le suivant</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="328"/>
        <location filename="../core/MainWindow.ui" line="331"/>
        <source>Find previous</source>
        <translation>Trouver le précédent</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="obsolete">Back</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="343"/>
        <source>Go back</source>
        <translation>Revenir en arrière</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="obsolete">Forward</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="363"/>
        <source>Unlock Panels</source>
        <translation>Déverrouiller les panneaux</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="366"/>
        <source>Toggle panel locks</source>
        <translation>Activer/désactiver les verrous du panneau</translation>
    </message>
    <message>
        <source>Lock/Unlock</source>
        <translation type="vanished">Verrouiller/Déverrouiller</translation>
    </message>
    <message>
        <source>Strings</source>
        <translation type="vanished">Chaînes de caractères</translation>
    </message>
    <message>
        <source>Show/Hide Strings panel</source>
        <translation type="vanished">Afficher/Masquer le panel des strings</translation>
    </message>
    <message>
        <source>Sections</source>
        <translation type="vanished">Sections</translation>
    </message>
    <message>
        <source>Show/Hide Sections panel</source>
        <translation type="vanished">Afficher/Masquer le panel des sections</translation>
    </message>
    <message>
        <source>Segments</source>
        <translation type="vanished">Segments</translation>
    </message>
    <message>
        <source>Show/Hide Segments panel</source>
        <translation type="vanished">Afficher/Masquer le panel des segments</translation>
    </message>
    <message>
        <source>Functions</source>
        <translation type="vanished">Fonctions</translation>
    </message>
    <message>
        <source>Show/Hide Functions panel</source>
        <translation type="vanished">Afficher/Masquer le panel des fonctions</translation>
    </message>
    <message>
        <source>Imports</source>
        <translation type="vanished">Imports</translation>
    </message>
    <message>
        <source>Show/Hide Imports panel</source>
        <translation type="vanished">Afficher/Masquer le panel des imports</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="vanished">Symboles</translation>
    </message>
    <message>
        <source>Show/Hide Symbols panel</source>
        <translation type="vanished">Afficher/Masquer le panel des symboles</translation>
    </message>
    <message>
        <source>Relocs</source>
        <translation type="vanished">Relocs</translation>
    </message>
    <message>
        <source>Show/Hide Relocs panel</source>
        <translation type="vanished">Afficher/Masquer le panel des relocs</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="vanished">Drapeaux</translation>
    </message>
    <message>
        <source>Show/Hide Flags panel</source>
        <translation type="vanished">Afficher/Masquer le panel des flags</translation>
    </message>
    <message>
        <source>Memory</source>
        <translation type="vanished">Mémoire</translation>
    </message>
    <message>
        <source>Show/Hide Memory panel</source>
        <translation type="vanished">Afficher/Masquer le panel de la mémoire</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="385"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="400"/>
        <location filename="../core/MainWindow.ui" line="403"/>
        <source>Tabs up/down</source>
        <translation>Onglets haut/bas</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="412"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <source>Comments</source>
        <translation type="vanished">Commentaires</translation>
    </message>
    <message>
        <source>Show/Hide comments</source>
        <translation type="vanished">Afficher/Masquer les commentaires</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="420"/>
        <source>Show Tabs at the Top</source>
        <translation>Afficher les onglets en haut</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="423"/>
        <source>Toggle tab position</source>
        <translation>Basculer la position des onglets</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="428"/>
        <source>Dark Theme</source>
        <translation>Thème Sombre</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="433"/>
        <location filename="../core/MainWindow.ui" line="436"/>
        <source>Load layout</source>
        <translation>Charger la disposition</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="441"/>
        <source>Default Theme</source>
        <translation>Thème par défaut</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="446"/>
        <source>Bindiff</source>
        <translation>BinDiff</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="451"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="456"/>
        <source>Test menu</source>
        <translation>Menu de test</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="461"/>
        <location filename="../core/MainWindow.ui" line="464"/>
        <source>Copy hexpair</source>
        <translation>Copier la paire hexadécimale</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="469"/>
        <location filename="../core/MainWindow.ui" line="472"/>
        <source>Copy text</source>
        <translation>Copier le texte</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="477"/>
        <source>Copy ASCII</source>
        <translation>Copier l&apos;ASCII</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="482"/>
        <location filename="../core/MainWindow.ui" line="485"/>
        <source>Insert string</source>
        <translation>Insérer une chaine de caractères</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="490"/>
        <location filename="../core/MainWindow.ui" line="493"/>
        <source>Insert hex</source>
        <translation>Insérer de l&apos;hexadécimal</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="508"/>
        <source>Show/Hide bytes</source>
        <translation>Afficher/Masquer les octets</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="513"/>
        <source>Switch case</source>
        <translation>Switch case</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="518"/>
        <location filename="../core/MainWindow.ui" line="521"/>
        <source>Copy all</source>
        <translation>Tout copier</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="526"/>
        <location filename="../core/MainWindow.ui" line="529"/>
        <source>Copy bytes</source>
        <translation>Copier les octets</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="534"/>
        <location filename="../core/MainWindow.ui" line="537"/>
        <location filename="../core/MainWindow.ui" line="542"/>
        <location filename="../core/MainWindow.ui" line="545"/>
        <source>Copy disasm</source>
        <translation>Copier l&apos;asm</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="557"/>
        <location filename="../core/MainWindow.ui" line="560"/>
        <source>Start web server</source>
        <translation>Démarrer le serveur web</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="565"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="570"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="575"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="580"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="585"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="590"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="595"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="600"/>
        <source>Syntax AT&amp;T/Intel</source>
        <translation>Syntaxe AT&amp;T/Intel</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="605"/>
        <location filename="../core/MainWindow.ui" line="615"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="610"/>
        <location filename="../core/MainWindow.ui" line="620"/>
        <source>Undefine</source>
        <translation>Non défini</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="625"/>
        <source>Add comment</source>
        <translation>Ajouter un commentaire</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="637"/>
        <location filename="../core/MainWindow.ui" line="640"/>
        <source>Show/Hide bottom pannel</source>
        <translation>Afficher/Masquer le panel du bas</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="645"/>
        <source>Run Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="717"/>
        <source>Save Project As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="735"/>
        <source>Analyze Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="809"/>
        <source>Commit changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="817"/>
        <source>Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="820"/>
        <source>Open the file in write mode. Every change to the file will change the original file on disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="828"/>
        <source>Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="831"/>
        <source>Enable cache mode. Changes to the file would not be applied to disk unless you specifically commit them. This is a safer option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="842"/>
        <source>Read-Only mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="852"/>
        <source>Manage layouts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SDB Browser</source>
        <translation type="vanished">Navigateur SDB</translation>
    </message>
    <message>
        <source>Run Script</source>
        <translation type="vanished">Exécuter le script</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="vanished">Tableau de bord</translation>
    </message>
    <message>
        <source>Show/Hide Dashboard panel</source>
        <translation type="vanished">Afficher/Masquer le tableau de bord</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="650"/>
        <source>Reset Settings</source>
        <translation>Réinitialiser les paramètres</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="653"/>
        <source>Reset settings</source>
        <translation>Réinitialiser les paramètres</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="658"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="661"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="669"/>
        <source>Exports</source>
        <translation>Exportations</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="672"/>
        <source>Show/Hide Exports panel</source>
        <translation>Afficher/Masquer le panel des exports</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="677"/>
        <source>Refresh Contents</source>
        <translation>Actualiser le contenu</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="680"/>
        <source>Refresh contents</source>
        <translation>Actualiser le contenu</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="688"/>
        <source>Show ESIL rather than assembly</source>
        <translation>Afficher ESIL plutôt que l&apos;assembleur</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="696"/>
        <source>Show pseudocode rather than assembly</source>
        <translation>Afficher le pseudocode plutôt que l&apos;assembleur</translation>
    </message>
    <message>
        <source>Entry Points</source>
        <translation type="vanished">Points d’entrée</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="707"/>
        <source>Display offsets</source>
        <translation>Afficher les offsets</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="712"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="vanished">Enregistrer sous...</translation>
    </message>
    <message>
        <source>Graph Overview</source>
        <translation type="vanished">Aperçu du graphe</translation>
    </message>
    <message>
        <source>Decompiler</source>
        <translation type="vanished">Décompileur</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="745"/>
        <source>Add Hexdump</source>
        <translation>Ajouter un dump Hexadécimal</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="750"/>
        <source>Add Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="755"/>
        <source>Add Disassembly</source>
        <translation>Ajouter un désassemblage</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="760"/>
        <source>Add Graph</source>
        <translation>Ajouter un graphe</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="771"/>
        <source>Grouped dock dragging</source>
        <translation>Déplacement du dock groupé</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="776"/>
        <source>Zoom In</source>
        <translation>Zoom avant</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="779"/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="787"/>
        <source>Zoom Out</source>
        <translation>Zoom Arrière</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="790"/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="798"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="801"/>
        <source>Ctrl+=</source>
        <translation>Ctrl+=</translation>
    </message>
    <message>
        <source>Tmp</source>
        <translation type="obsolete">Tmp</translation>
    </message>
    <message>
        <source>Disassembly</source>
        <translation type="vanished">Vue Désassembleur</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="725"/>
        <source>Graph</source>
        <translation>Graphe</translation>
    </message>
    <message>
        <source>Pseudocode</source>
        <translation type="obsolete">Pseudocode</translation>
    </message>
    <message>
        <source>Hexdump</source>
        <translation type="obsolete">Hexdump</translation>
    </message>
    <message>
        <source>Sidebar</source>
        <translation type="obsolete">Sidebar</translation>
    </message>
    <message>
        <source>Console</source>
        <translation type="vanished">Console</translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="vanished">Pile</translation>
    </message>
    <message>
        <source>Registers</source>
        <translation type="vanished">Registres</translation>
    </message>
    <message>
        <source>Backtrace</source>
        <translation type="vanished">Trace d&apos;appels</translation>
    </message>
    <message>
        <source>Threads</source>
        <translation type="vanished">Threads</translation>
    </message>
    <message>
        <source>Processes</source>
        <translation type="vanished">Processus</translation>
    </message>
    <message>
        <source>Memory map</source>
        <translation type="vanished">Sections mémoire</translation>
    </message>
    <message>
        <source>Breakpoints</source>
        <translation type="vanished">Points d&apos;arrêt</translation>
    </message>
    <message>
        <source>Register References</source>
        <translation type="vanished">Références aux registres</translation>
    </message>
    <message>
        <source>Classes</source>
        <translation type="vanished">Classes</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="730"/>
        <source>Import PDB</source>
        <translation>Importer PDB</translation>
    </message>
    <message>
        <source>Analyze</source>
        <translation type="vanished">Analyser</translation>
    </message>
    <message>
        <source>Resources</source>
        <translation type="vanished">Ressources</translation>
    </message>
    <message>
        <source>VTables</source>
        <translation type="vanished">Tables virtuelles</translation>
    </message>
    <message>
        <source>Show/Hide VTables panel</source>
        <translation type="vanished">Afficher/Masquer le panel des VTables</translation>
    </message>
    <message>
        <source>Types</source>
        <translation type="vanished">Types</translation>
    </message>
    <message>
        <source>Show/Hide Types panel</source>
        <translation type="vanished">Afficher/Masquer le panel des types</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Rechercher</translation>
    </message>
    <message>
        <source>Show/Hide Search panel</source>
        <translation type="vanished">Afficher/Masquer le panel de recherche</translation>
    </message>
    <message>
        <source>Headers</source>
        <translation type="vanished">En-têtes</translation>
    </message>
    <message>
        <source>Show/Hide Headers panel</source>
        <translation type="vanished">Afficher/Masquer le panel des entêtes</translation>
    </message>
    <message>
        <source>Zignatures</source>
        <translation type="vanished">Zignatures</translation>
    </message>
    <message>
        <source>Show/Hide Zignatures panel</source>
        <translation type="vanished">Afficher/Masquer le panel Zignatures</translation>
    </message>
    <message>
        <source>Jupyter</source>
        <translation type="obsolete">Jupyter</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.ui" line="740"/>
        <location filename="../core/MainWindow.cpp" line="1697"/>
        <source>Export as code</source>
        <translation>Exporter en tant que code</translation>
    </message>
    <message>
        <source>Hexdump view</source>
        <translation type="obsolete">Hexdump view</translation>
    </message>
    <message>
        <source>Disassembly view</source>
        <translation type="obsolete">Disassembly view</translation>
    </message>
    <message>
        <source>Graph view</source>
        <translation type="obsolete">Graph view</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="533"/>
        <source>Script loading</source>
        <translation>Chargement du script</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="534"/>
        <source>Do you want to load the &apos;%1&apos; script?</source>
        <translation>Voulez-vous charger le script &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="552"/>
        <source>Cannot open file!</source>
        <translation>Impossible d&apos;ouvrir le fichier!</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="553"/>
        <source>Could not open the file! Make sure the file exists and that you have the correct permissions.</source>
        <translation>Impossible d&apos;ouvrir le fichier ! Assurez-vous que le fichier existe et que vous avez les permissions correctes.</translation>
    </message>
    <message>
        <source> &gt; Populating UI</source>
        <translation type="obsolete"> &gt; Populating UI</translation>
    </message>
    <message>
        <source> &gt; Finished, happy reversing :)</source>
        <translation type="obsolete"> &gt; Finished, happy reversing :)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="764"/>
        <source>Do you really want to exit?
Save your project before closing!</source>
        <translation>Voulez-vous vraiment quitter ?
Enregistrez votre projet avant de le fermer!</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1038"/>
        <source>New disassembly</source>
        <translation>Nouveau désassemblage</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1040"/>
        <source>New graph</source>
        <translation>Nouveau graphe</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1042"/>
        <source>New hexdump</source>
        <translation>Nouveau dump hexadécimal</translation>
    </message>
    <message>
        <source>Select radare2 script</source>
        <translation type="vanished">Sélectionnez le script radare2</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="251"/>
        <source>No plugins are installed. Check the plugins section on Cutter documentation to learn more.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="256"/>
        <source>The installed plugins didn&apos;t add entries to this menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="616"/>
        <source>Failed to open project: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="619"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1043"/>
        <source>New Decompiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1204"/>
        <source>Save layout error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1205"/>
        <source>&apos;%1&apos; is not a valid name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1207"/>
        <source>Enter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1514"/>
        <source>Select Rizin script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1557"/>
        <source>Do you really want to clear all settings?</source>
        <translation>Voulez-vous vraiment rétablir les paramètres par défaut ?</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1651"/>
        <source>Select PDB file</source>
        <translation>Sélectionner un fichier PDB</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1652"/>
        <source>PDB file (*.pdb)</source>
        <translation>Fichier PDB (*.pdb)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1652"/>
        <source>All files (*)</source>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1662"/>
        <source>%1 loaded.</source>
        <translation>%1 chargé.</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1672"/>
        <source>C uin8_t array (*.c)</source>
        <translation>Tableau C uin8_t (*.c)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1674"/>
        <source>C uin16_t array (*.c)</source>
        <translation>Tableau C uin16_t (*.c)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1676"/>
        <source>C uin32_t array (*.c)</source>
        <translation>Tableau C uin32_t (*.c)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1678"/>
        <source>C uin64_t array (*.c)</source>
        <translation>Tableau C uin64_t (*.c)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1680"/>
        <source>C string (*.c)</source>
        <translation>Chaîne C (*.c)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1682"/>
        <source>Shell-script that reconstructs the bin (*.sh)</source>
        <translation>Script shell qui reconstruit le binaire (*.sh)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1684"/>
        <source>JSON array (*.json)</source>
        <translation>Tableau JSON (*.json)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1686"/>
        <source>JavaScript array (*.js)</source>
        <translation>Tableau JavaScript (*.js)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1688"/>
        <source>Python array (*.py)</source>
        <translation>Tableau Python (*.py)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1690"/>
        <source>Print &apos;wx&apos; Rizin commands (*.rz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print &apos;wx&apos; r2 commands (*.r2)</source>
        <translation type="vanished">Imprimer &apos;wx&apos; les commandes r2 (*.r2)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1692"/>
        <source>GAS .byte blob (*.asm, *.s)</source>
        <translation>Blob GAS .octet (*.asm, *.s)</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="1694"/>
        <source>.bytes with instructions in comments (*.txt)</source>
        <translation>.octets avec instructions dans les commentaires (*.txt)</translation>
    </message>
    <message>
        <source>Project saved: %1</source>
        <translation type="vanished">Projet sauvegardé: %1</translation>
    </message>
    <message>
        <location filename="../core/MainWindow.cpp" line="736"/>
        <source>Failed to save project: %1</source>
        <translation>Impossible d&apos;enregistrer le projet : %1</translation>
    </message>
    <message>
        <source>Project saved:</source>
        <translation type="obsolete">Project saved:</translation>
    </message>
</context>
<context>
    <name>MapFileDialog</name>
    <message>
        <location filename="../dialogs/MapFileDialog.ui" line="14"/>
        <source>Map New File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/MapFileDialog.ui" line="26"/>
        <source>File:</source>
        <translation type="unfinished">Fichier:</translation>
    </message>
    <message>
        <location filename="../dialogs/MapFileDialog.ui" line="52"/>
        <location filename="../dialogs/MapFileDialog.cpp" line="18"/>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/MapFileDialog.ui" line="59"/>
        <source>Map address:</source>
        <translation type="unfinished">Tracer l&apos;adresse:</translation>
    </message>
    <message>
        <location filename="../dialogs/MapFileDialog.ui" line="72"/>
        <source>0x40000</source>
        <translation type="unfinished">0x40000</translation>
    </message>
    <message>
        <location filename="../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Map new file file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/MapFileDialog.cpp" line="36"/>
        <source>Failed to map a new file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryDockWidget</name>
    <message>
        <source>Sync/unsync offset</source>
        <translation type="vanished">Synchroniser/désynchroniser l&apos;offset</translation>
    </message>
</context>
<context>
    <name>MemoryMapModel</name>
    <message>
        <location filename="../widgets/MemoryMapWidget.cpp" line="58"/>
        <source>Offset start</source>
        <translation>Début de l&apos;offset</translation>
    </message>
    <message>
        <location filename="../widgets/MemoryMapWidget.cpp" line="60"/>
        <source>Offset end</source>
        <translation>Fin de l&apos;offset</translation>
    </message>
    <message>
        <location filename="../widgets/MemoryMapWidget.cpp" line="62"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/MemoryMapWidget.cpp" line="64"/>
        <source>Permissions</source>
        <translation>Autorisations</translation>
    </message>
    <message>
        <location filename="../widgets/MemoryMapWidget.cpp" line="66"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>MemoryMapWidget</name>
    <message>
        <location filename="../widgets/MemoryMapWidget.cpp" line="123"/>
        <source>Memory Map</source>
        <translation>Sections mémoire</translation>
    </message>
</context>
<context>
    <name>MultitypeFileSaveDialog</name>
    <message>
        <location filename="../dialogs/MultitypeFileSaveDialog.cpp" line="24"/>
        <source>Detect type (*)</source>
        <translation>Supprimez le type (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/MultitypeFileSaveDialog.cpp" line="63"/>
        <source>File save error</source>
        <translation>Erreur lors de l&apos;enregistrement du fichier</translation>
    </message>
    <message>
        <location filename="../dialogs/MultitypeFileSaveDialog.cpp" line="64"/>
        <source>Unrecognized extension &apos;%1&apos;</source>
        <translation>Extension &apos;%1&apos; non reconnues</translation>
    </message>
</context>
<context>
    <name>NativeDebugDialog</name>
    <message>
        <location filename="../dialogs/NativeDebugDialog.ui" line="35"/>
        <source>Command line arguments:</source>
        <translation>Arguments de la ligne de commande:</translation>
    </message>
</context>
<context>
    <name>NewFileDialog</name>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="20"/>
        <location filename="../dialogs/NewFileDialog.ui" line="149"/>
        <source>Open File</source>
        <translation>Ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="79"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="182"/>
        <location filename="../dialogs/NewFileDialog.ui" line="390"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="169"/>
        <source>&lt;b&gt;Select new file&lt;b&gt;</source>
        <translation>&lt;b&gt;Sélectionner un nouveau fichier&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="202"/>
        <source>&lt;b&gt;IO&lt;/b&gt;</source>
        <translation>&lt;b&gt;IO&lt;/b&gt;</translation>
    </message>
    <message>
        <source>://</source>
        <translation type="vanished">://</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="296"/>
        <source>Don&apos;t open any file</source>
        <translation>Don&apos;t ouvre n&apos;importe quel fichier</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="303"/>
        <location filename="../dialogs/NewFileDialog.ui" line="360"/>
        <location filename="../dialogs/NewFileDialog.ui" line="501"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="316"/>
        <source>Open Shellcode</source>
        <translation>Ouvrir le Shellcode</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="328"/>
        <source>&lt;b&gt;Paste Shellcode&lt;b&gt;</source>
        <translation>&lt;b&gt;Coller le shellcode&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="370"/>
        <source>Projects</source>
        <translation>Projets</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="410"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Open Project&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="534"/>
        <source>Clear all projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Projects path (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Chemin des projets (dir.projects):&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="519"/>
        <source>Remove item</source>
        <translation>Supprimer l&apos;élément</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="524"/>
        <source>Clear all</source>
        <translation>Tout nettoyer</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.ui" line="529"/>
        <source>Delete project</source>
        <translation>Supprimer le projet</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.cpp" line="92"/>
        <source>Select file</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">Sélectionner le chemin du projet (dir.projects)</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">Autorisation refusée</translation>
    </message>
    <message>
        <source>You do not have write access to &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">Vous n&apos;avez pas d&apos;accès en écriture à &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Delete the project &quot;%1&quot; from disk ?</source>
        <translation type="vanished">Supprimer le projet &quot;%1&quot; du disque ?</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.cpp" line="104"/>
        <source>Open Project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.cpp" line="287"/>
        <source>Open a file with no extra treatment.</source>
        <translation>Ouvrir un fichier sans traitement supplémentaire.</translation>
    </message>
    <message>
        <location filename="../dialogs/NewFileDialog.cpp" line="319"/>
        <source>Select a new program or a previous one before continuing.</source>
        <translation>Sélectionnez un nouveau programme ou un programme précédent avant de continuer.</translation>
    </message>
</context>
<context>
    <name>Omnibar</name>
    <message>
        <location filename="../widgets/Omnibar.cpp" line="15"/>
        <source>Type flag name or address here</source>
        <translation>Tapez le nom ou l&apos;adresse du flag ici</translation>
    </message>
</context>
<context>
    <name>OpenFileDialog</name>
    <message>
        <source>Open file</source>
        <translation type="vanished">Ouvrir un fichier</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="vanished">Sélectionner un fichier</translation>
    </message>
    <message>
        <source>Map address:</source>
        <translation type="vanished">Tracer l&apos;adresse:</translation>
    </message>
    <message>
        <source>File:</source>
        <translation type="vanished">Fichier:</translation>
    </message>
    <message>
        <source>Map address</source>
        <translation type="obsolete">Map address</translation>
    </message>
    <message>
        <source>0x40000</source>
        <translation type="vanished">0x40000</translation>
    </message>
    <message>
        <source>Failed to open file</source>
        <translation type="vanished">Impossible d&apos;ouvrir le fichier</translation>
    </message>
</context>
<context>
    <name>PluginsOptionsWidget</name>
    <message>
        <source>Plugins are loaded from &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="vanished">Les plugins sont chargés à partir de &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PluginsOptionsWidget.cpp" line="28"/>
        <source>Plugins are loaded from &lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PluginsOptionsWidget.cpp" line="34"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PluginsOptionsWidget.cpp" line="48"/>
        <source>Show Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show radare2 plugin information</source>
        <translation type="vanished">Afficher les informations du plugin radare2</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../dialogs/preferences/PreferencesDialog.ui" line="6"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PreferencesDialog.cpp" line="28"/>
        <source>Disassembly</source>
        <translation>Vue Désassembleur</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PreferencesDialog.cpp" line="34"/>
        <source>Debug</source>
        <translation>Débogage</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PreferencesDialog.cpp" line="35"/>
        <source>Appearance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PreferencesDialog.cpp" line="36"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PreferencesDialog.cpp" line="37"/>
        <source>Initialization Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/preferences/PreferencesDialog.cpp" line="39"/>
        <source>Analysis</source>
        <translation type="unfinished">Analyse</translation>
    </message>
</context>
<context>
    <name>ProcessModel</name>
    <message>
        <location filename="../dialogs/AttachProcDialog.cpp" line="71"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../dialogs/AttachProcDialog.cpp" line="73"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../dialogs/AttachProcDialog.cpp" line="75"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../dialogs/AttachProcDialog.cpp" line="77"/>
        <source>Path</source>
        <translation>Chemin d&apos;accès</translation>
    </message>
</context>
<context>
    <name>ProcessesWidget</name>
    <message>
        <location filename="../widgets/ProcessesWidget.cpp" line="26"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../widgets/ProcessesWidget.cpp" line="27"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../widgets/ProcessesWidget.cpp" line="28"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../widgets/ProcessesWidget.cpp" line="29"/>
        <source>Path</source>
        <translation>Chemin d&apos;accès</translation>
    </message>
    <message>
        <location filename="../widgets/ProcessesWidget.cpp" line="172"/>
        <source>Unable to switch to the requested process.</source>
        <translation>Impossible de passer au processus demandé.</translation>
    </message>
</context>
<context>
    <name>PseudocodeWidget</name>
    <message>
        <source>Pseudocode</source>
        <translation type="obsolete">Pseudocode</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Refresh</translation>
    </message>
    <message>
        <source>Decompiler:</source>
        <translation type="obsolete">Decompiler:</translation>
    </message>
    <message>
        <source>r2dec</source>
        <translation type="obsolete">r2dec</translation>
    </message>
    <message>
        <source>pdc</source>
        <translation type="obsolete">pdc</translation>
    </message>
    <message>
        <source>Click Refresh to generate Pseudocode from current offset.</source>
        <translation type="obsolete">Click Refresh to generate Pseudocode from current offset.</translation>
    </message>
    <message>
        <source>Cannot decompile at</source>
        <translation type="obsolete">Cannot decompile at</translation>
    </message>
    <message>
        <source>(Not a function?)</source>
        <translation type="obsolete">(Not a function?)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>A Qt and C++ GUI for radare2 reverse engineering framework</source>
        <translation type="vanished">Une interface de Reverse Engineering utilisant Qt et C++ pour radare2</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="97"/>
        <source>The version used to compile Cutter (%1) does not match the binary version of rizin (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="317"/>
        <source>A Qt and C++ GUI for rizin reverse engineering framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="320"/>
        <source>Filename to open.</source>
        <translation>Nom de fichier à ouvrir.</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="324"/>
        <source>Automatically open file and optionally start analysis. Needs filename to be specified. May be a value between 0 and 2: 0 = no analysis, 1 = aaa, 2 = aaaa (experimental)</source>
        <translation>Ouvre automatiquement le fichier et lance l&apos;analyse (optionnel). Il faut spécifier le nom du fichier. La valeur doit être comprise entre 0 et 2: 0 = aucune analyse, 1 = aaa, 2 = aaaa (expérimental)</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="327"/>
        <source>level</source>
        <translation>niveau</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="331"/>
        <source>Force using a specific file format (bin plugin)</source>
        <translation>Forcer l&apos;utilisation d&apos;un format de fichier spécifique (plugin bin)</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="332"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="336"/>
        <source>Load binary at a specific base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="337"/>
        <source>base address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="340"/>
        <source>Run script file</source>
        <translation>Lancer le fichier de script</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="340"/>
        <source>file</source>
        <translation>fichier</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="343"/>
        <source>Load project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="344"/>
        <source>project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="348"/>
        <source>Open file in write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="352"/>
        <source>PYTHONHOME to use for embedded python interpreter</source>
        <translation>PYTHONHOME à utiliser pour l&apos;interpréteur python intégré</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="358"/>
        <source>Disable output redirection. Some of the output in console widget will not be visible. Use this option when debuging a crash or freeze and output  redirection is causing some messages to be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="364"/>
        <source>Do not load plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="368"/>
        <source>Do not load Cutter plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="372"/>
        <source>Do not load rizin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PYTHONHOME to use for Jupyter</source>
        <translation type="obsolete">PYTHONHOME to use for Jupyter</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="96"/>
        <source>Version mismatch!</source>
        <translation>Incompatibilité de version!</translation>
    </message>
    <message>
        <source>The version used to compile Cutter (%1) does not match the binary version of radare2 (%2). This could result in unexpected behaviour. Are you sure you want to continue?</source>
        <translation type="vanished">La version utilisée pour compiler Cutter (%1) ne correspond pas à la version binaire de radare2 (%2). Cela peut entraîner un comportement inattendu. Êtes-vous sûr de vouloir continuer ?</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="386"/>
        <source>Invalid Analysis Level. May be a value between 0 and 2.</source>
        <translation>Niveau d&apos;analyse invalide. La valeur doit être comprise entre 0 et 2.</translation>
    </message>
    <message>
        <location filename="../CutterApplication.cpp" line="406"/>
        <source>Filename must be specified to start analysis automatically.</source>
        <translation>Le nom du fichier doit être spécifié pour démarrer l&apos;analyse automatiquement.</translation>
    </message>
    <message>
        <source>Color of comment generated by radare2</source>
        <translation type="vanished">Couleur du commentaire généré par radare2</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="410"/>
        <source>Color of comment generated by Rizin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Comment created by user</source>
        <translation>Commentaire créé par l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="411"/>
        <source>Color of user Comment</source>
        <translation>Couleur du commentaire de l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Color of function arguments</source>
        <translation type="obsolete">Color of function arguments</translation>
    </message>
    <message>
        <source>Arguments</source>
        <translation type="obsolete">Arguments</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Color of names of functions</source>
        <translation>Couleur des noms des fonctions</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="413"/>
        <source>Function name</source>
        <translation>Nom de la fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Color of function location</source>
        <translation>Couleur de l&apos;emplacement de la fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="414"/>
        <source>Function location</source>
        <translation>Emplacement de la fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="416"/>
        <source>Color of ascii line in left side that shows what opcodes are belong to function</source>
        <translation>Couleur de la ligne ascii sur le côté gauche qui montre quels codes d&apos;opérations appartiennent à la fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="418"/>
        <source>Function line</source>
        <translation>Ligne de fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Color of flags (similar to bookmarks for offset)</source>
        <translation>Couleur des flags (similaires aux signets pour le décalage)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="420"/>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="421"/>
        <source>Label</source>
        <translation>Étiquette</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="422"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>flow</source>
        <translation type="obsolete">flow</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="424"/>
        <source>flow2</source>
        <translation>flux2</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="425"/>
        <location filename="../widgets/ColorThemeListView.cpp" line="427"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="425"/>
        <source>prompt</source>
        <translation>prompt</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Color of offsets</source>
        <translation>Couleur des offsets</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="426"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="427"/>
        <source>input</source>
        <translation>entrée</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="428"/>
        <source>Invalid opcode color</source>
        <translation>Couleur de code d&apos;opération invalide</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="428"/>
        <source>invalid</source>
        <translation>invalide</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="429"/>
        <source>other</source>
        <translation>autres</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="430"/>
        <source>0x00 opcode color</source>
        <translation>couleur du code d&apos;opération 0x00</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="431"/>
        <source>0x7f opcode color</source>
        <translation>couleur du code d&apos;opération 0x7f</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="432"/>
        <source>0xff opcode color</source>
        <translation>couleur du code d&apos;opération 0xff</translation>
    </message>
    <message>
        <source>arithmetic color (+, -, *, / etc.)</source>
        <translation type="obsolete">arithmetic color (+, -, *, / etc.)</translation>
    </message>
    <message>
        <source>bin</source>
        <translation type="obsolete">bin</translation>
    </message>
    <message>
        <source>btext</source>
        <translation type="obsolete">btext</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="443"/>
        <source>push opcode color</source>
        <translation>couleur du code d&apos;opération push</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="444"/>
        <source>pop opcode color</source>
        <translation>couleur du code d&apos;opération pop</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="445"/>
        <source>Cryptographic color</source>
        <translation>Couleur cryptographique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="446"/>
        <source>jmp instructions color</source>
        <translation>couleur des instructions jmp</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="450"/>
        <source>call instructions color (ccall, rcall, call etc)</source>
        <translation>appeler les instructions de couleur (ccall, rcall, call...)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="451"/>
        <source>nop opcode color</source>
        <translation>couleur du code d&apos;opération nop</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="452"/>
        <source>ret opcode color</source>
        <translation>couleur du code d&apos;opération ret</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Color of interrupts</source>
        <translation>Couleur des interruptions</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="453"/>
        <source>Interrupts</source>
        <translation>Interruptions</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="454"/>
        <source>swi opcode color</source>
        <translation>couleur du code d&apos;opération swi</translation>
    </message>
    <message>
        <source>cmp opcode color</source>
        <translation type="obsolete">cmp opcode color</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Registers color</source>
        <translation>Couleur des registres</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="458"/>
        <source>Register</source>
        <translation>Registre</translation>
    </message>
    <message>
        <source>Numeric constants color</source>
        <translation type="obsolete">Numeric constants color</translation>
    </message>
    <message>
        <source>Numbers</source>
        <translation type="obsolete">Numbers</translation>
    </message>
    <message>
        <source>mov instructions color (mov, movd, movw etc</source>
        <translation type="obsolete">mov instructions color (mov, movd, movw etc</translation>
    </message>
    <message>
        <source>mov</source>
        <translation type="vanished">mov</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Color of lines showing jump destination</source>
        <translation>Couleur des lignes affichant la destination du saut</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="423"/>
        <source>Flow</source>
        <translation>Flux</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="434"/>
        <source>Color of arithmetic opcodes (add, div, mul etc)</source>
        <translation>Couleur des opcodes arithmétiques (ajout, div, mul...)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="435"/>
        <source>Arithmetic</source>
        <translation>Arithmétique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Color of binary operations (and, or, xor etc).</source>
        <translation>Couleur des opérations binaires (and, or, xor...)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="437"/>
        <source>Binary</source>
        <translation>Binaire</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="439"/>
        <source>Color of object names, commas between operators, squared brackets and operators inside them.</source>
        <translation>Couleur des noms d&apos;objets, des virgules entre les opérateurs, les crochets et les opérateurs à l&apos;intérieur d&apos;eux.</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="442"/>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="448"/>
        <source>Color of conditional jump opcodes such as je, jg, jne etc</source>
        <translation>Couleur des opcodes de saut conditionnels (je, jg, jne...)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="449"/>
        <source>Conditional jump</source>
        <translation>Saut conditionnel</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="456"/>
        <source>Color of compare instructions such as test and cmp</source>
        <translation>Couleur des instructions de comparaison (test / cmp)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="457"/>
        <source>Compare instructions</source>
        <translation>Comparer les instructions</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Color of numeric constants and object pointers</source>
        <translation>Couleur des constantes numériques et des pointeurs d&apos;objet</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="461"/>
        <source>Constants</source>
        <translation>Constantes</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="463"/>
        <source>Color of move instructions such as mov, movd, lea etc</source>
        <translation>Couleur des instructions de déplacement (mov, movd, lea...)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="464"/>
        <source>Move instructions</source>
        <translation>Instructions de déplacement</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable color</source>
        <translation>Couleur de la variable de fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="465"/>
        <source>Function variable</source>
        <translation>Variable de fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="467"/>
        <source>Function variable (local or argument) type color</source>
        <translation>Couleur de la variable de fonction (local ou argument)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="468"/>
        <source>Variable type</source>
        <translation>Type de variable</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Function variable address color</source>
        <translation>Couleur d&apos;adresse de la variable de fonction</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="470"/>
        <source>Variable address</source>
        <translation>Adresse variable</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="482"/>
        <source>In graph view jump arrow true</source>
        <translation>Dans la vue graphique flèche de saut vrai</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="482"/>
        <source>Arrow true</source>
        <translation>Flèche vraie</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="484"/>
        <source>In graph view jump arrow false</source>
        <translation>Flèche de saut en vue graphique fausse</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="484"/>
        <source>Arrow false</source>
        <translation>Flèche fausse</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="486"/>
        <source>In graph view jump arrow (no condition)</source>
        <translation>Dans la vue graphique flèche de saut (sans condition)</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="486"/>
        <source>Arrow</source>
        <translation>Flèche</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="490"/>
        <source>Background color of Graph Overview&apos;s node</source>
        <translation>Couleur d&apos;arrière-plan du nœud de la vue du graphique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="491"/>
        <source>Graph Overview node</source>
        <translation>Nœud d&apos;aperçu du graphique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="493"/>
        <source>Fill color of Graph Overview&apos;s selection</source>
        <translation>Remplir la couleur de la sélection de la vue du graphique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="494"/>
        <source>Graph Overview fill</source>
        <translation>Remplissage de l&apos;aperçu du graphique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="496"/>
        <source>Border color of Graph Overview&apos;s selection</source>
        <translation>Couleur des bordures de la sélection de la vue graphique&apos;</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="497"/>
        <source>Graph Overview border</source>
        <translation>Bordure de présentation graphique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="500"/>
        <source>General background color</source>
        <translation>Couleur générale d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="500"/>
        <source>Background</source>
        <translation>Arrière-plan</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="502"/>
        <source>Background color of non-focused graph node</source>
        <translation>Couleur d&apos;arrière-plan du nœud graphique non ciblé</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="503"/>
        <source>Node background</source>
        <translation>Arrière-plan du noeud</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Background color of selected word</source>
        <translation>Couleur d&apos;arrière-plan du mot sélectionné</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main function color</source>
        <translation>Couleur de la fonction principale</translation>
    </message>
    <message>
        <source>Alt. background</source>
        <translation type="obsolete">Alt. background</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Background of current graph node</source>
        <translation>Arrière-plan du nœud graphique actuel</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="505"/>
        <source>Current graph node</source>
        <translation>Noeud graphique actuel</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Color of node border in graph view</source>
        <translation>Couleur de la bordure du nœud dans la vue graphique</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="507"/>
        <source>Node border</source>
        <translation>Bordure du nœud</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Selected line background color</source>
        <translation>Couleur d&apos;arrière-plan de la ligne</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="509"/>
        <source>Line highlight</source>
        <translation>Mise en valeur de la ligne</translation>
    </message>
    <message>
        <source>Highlighted word text color</source>
        <translation type="obsolete">Highlighted word text color</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="511"/>
        <source>Word higlight</source>
        <translation>Mises en valeur du mot</translation>
    </message>
    <message>
        <source>Color of main function color</source>
        <translation type="obsolete">Color of main function color</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="512"/>
        <source>Main</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Code section color in navigation bar</source>
        <translation>Couleur de la section de code dans la barre de navigation</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="521"/>
        <source>Navbar code</source>
        <translation>Code de la barre de navigation</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Empty section color in navigation bar</source>
        <translation>Couleur de section vide dans la barre de navigation</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="523"/>
        <source>Navbar empty</source>
        <translation>Barre de navigation vide</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="524"/>
        <source>ucall</source>
        <translation>ucall</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="525"/>
        <source>ujmp</source>
        <translation>ujmp</translation>
    </message>
    <message>
        <location filename="../widgets/ColorThemeListView.cpp" line="526"/>
        <source>Breakpoint background</source>
        <translation>Point d&apos;arrêt d&apos;arrière-plan</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="100"/>
        <source>Crash</source>
        <translation>Plantage</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="101"/>
        <source>Cutter received a signal it can&apos;t handle and will close.&lt;br/&gt;Would you like to create a crash dump for a bug report?</source>
        <translation>Cutter a reçu un signal qu&apos;il ne peut pas gérer et va fermer.&lt;br/&gt;Voulez-vous créer un fichier de plantage pour un rapport de bug ?</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="104"/>
        <source>Create a Crash Dump</source>
        <translation>Créer un crash dump</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="105"/>
        <location filename="../common/CrashHandler.cpp" line="148"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="119"/>
        <source>Choose a directory to save the crash dump in</source>
        <translation>Choisissez un répertoire dans lequel enregistrer le fichier de plantage</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="124"/>
        <source>Minidump (*.dmp)</source>
        <translation>Minidump (*.dmp)</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="133"/>
        <source>Save Crash Dump</source>
        <translation>Enregistrer le fichier de plantage</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="134"/>
        <source>Failed to write to %1.&lt;br/&gt;Please make sure you have access to that directory and try again.</source>
        <translation>Impossible d&apos;écrire dans %1.&lt;br/&gt;Veuillez vous assurer que vous avez accès à ce répertoire et réessayez.</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="142"/>
        <source>Success</source>
        <translation>Succès</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="143"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; was successfully created.</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Crash dump&lt;/a&gt; a été créé avec succès.</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="147"/>
        <source>Open an Issue</source>
        <translation>Signaler un problème</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="156"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../common/CrashHandler.cpp" line="157"/>
        <source>Error occurred during crash dump creation.</source>
        <translation>Une erreur s&apos;est produite lors de la création du crash dump.</translation>
    </message>
    <message>
        <location filename="../common/IOModesController.cpp" line="53"/>
        <source>Write error</source>
        <translation type="unfinished">Erreur d&apos;écriture</translation>
    </message>
    <message>
        <location filename="../common/IOModesController.cpp" line="54"/>
        <source>Your file is opened in read-only mode. Editing is only available when the file is opened in either Write or Cache modes.

WARNING: In Write mode, any changes will be committed to the file on disk. For safety, please consider using Cache mode and then commit the changes manually via File -&gt; Commit modifications to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/IOModesController.cpp" line="60"/>
        <source>Cancel</source>
        <translation type="unfinished">Annuler</translation>
    </message>
    <message>
        <location filename="../common/IOModesController.cpp" line="62"/>
        <source>Reopen in Write mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/IOModesController.cpp" line="64"/>
        <source>Enable Cache mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/IOModesController.cpp" line="99"/>
        <source>Uncomitted changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../common/IOModesController.cpp" line="100"/>
        <source>It seems that you have changes or patches that are not committed to the file.
Do you want to commit them now?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickFilterView</name>
    <message>
        <location filename="../widgets/QuickFilterView.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../widgets/QuickFilterView.ui" line="41"/>
        <source>Quick Filter</source>
        <translation>Filtre rapide</translation>
    </message>
    <message>
        <location filename="../widgets/QuickFilterView.ui" line="61"/>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>R2DecDecompiler</name>
    <message>
        <source>Failed to parse JSON from r2dec</source>
        <translation type="vanished">Impossible d&apos;analyser le JSON depuis r2dec</translation>
    </message>
</context>
<context>
    <name>R2PluginsDialog</name>
    <message>
        <source>radare2 plugin information</source>
        <translation type="vanished">Informations sur le plugin radare2</translation>
    </message>
    <message>
        <source>RBin</source>
        <translation type="vanished">RBin</translation>
    </message>
    <message>
        <source>RBin plugins</source>
        <translation type="vanished">Plugins RBin</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Description</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Licence</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Type</translation>
    </message>
    <message>
        <source>RIO</source>
        <translation type="vanished">RIO</translation>
    </message>
    <message>
        <source>RIO plugins</source>
        <translation type="vanished">Plugins RIO</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Autorisations</translation>
    </message>
    <message>
        <source>RCore</source>
        <translation type="vanished">RCore</translation>
    </message>
    <message>
        <source>RCore plugins</source>
        <translation type="vanished">Plugins RCore</translation>
    </message>
    <message>
        <source>RAsm</source>
        <translation type="vanished">RAsm</translation>
    </message>
    <message>
        <source>RAsm plugins</source>
        <translation type="vanished">Plugins RAsm</translation>
    </message>
    <message>
        <source>Architecture</source>
        <translation type="vanished">Architecture</translation>
    </message>
    <message>
        <source>CPU&apos;s</source>
        <translation type="vanished">Processeur&apos;s</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="vanished">Version</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="vanished">Auteur</translation>
    </message>
    <message>
        <source>Cutter</source>
        <translation type="vanished">Cutter</translation>
    </message>
    <message>
        <source>Cutter plugins</source>
        <translation type="obsolete">Cutter plugins</translation>
    </message>
</context>
<context>
    <name>R2TaskDialog</name>
    <message>
        <source>R2 Task</source>
        <translation type="vanished">Tâche R2</translation>
    </message>
    <message>
        <source>R2 task in progress..</source>
        <translation type="vanished">Tâche R2 en cours...</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">Temps</translation>
    </message>
    <message>
        <source>Running for</source>
        <translation type="vanished">Actif depuis</translation>
    </message>
    <message numerus="yes">
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="vanished">
            <numerusform>%n heure</numerusform>
            <numerusform>%n heure(s)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="vanished">
            <numerusform>%n minute(s)</numerusform>
            <numerusform>%n minute(s)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="vanished">
            <numerusform>%n seconde(s)</numerusform>
            <numerusform>%n seconde(s)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RawAddrDock</name>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="500"/>
        <source>Raw</source>
        <translation>Ligne</translation>
    </message>
</context>
<context>
    <name>RegisterRefModel</name>
    <message>
        <location filename="../widgets/RegisterRefsWidget.cpp" line="67"/>
        <source>Register</source>
        <translation>Registrer</translation>
    </message>
    <message>
        <location filename="../widgets/RegisterRefsWidget.cpp" line="69"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../widgets/RegisterRefsWidget.cpp" line="71"/>
        <source>Reference</source>
        <translation>Référence</translation>
    </message>
    <message>
        <location filename="../widgets/RegisterRefsWidget.cpp" line="73"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>RegisterRefsWidget</name>
    <message>
        <location filename="../widgets/RegisterRefsWidget.cpp" line="137"/>
        <source>Copy register value</source>
        <translation>Copier la valeur du registre</translation>
    </message>
    <message>
        <location filename="../widgets/RegisterRefsWidget.cpp" line="138"/>
        <source>Copy register reference</source>
        <translation>Copier la référence du registre</translation>
    </message>
</context>
<context>
    <name>RelocsModel</name>
    <message>
        <location filename="../widgets/RelocsWidget.cpp" line="54"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../widgets/RelocsWidget.cpp" line="56"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/RelocsWidget.cpp" line="58"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/RelocsWidget.cpp" line="60"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>RelocsWidget</name>
    <message>
        <location filename="../widgets/RelocsWidget.cpp" line="131"/>
        <source>Relocs</source>
        <translation>Relocs</translation>
    </message>
</context>
<context>
    <name>RemoteDebugDialog</name>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.ui" line="43"/>
        <source>Debugger:</source>
        <translation>Débogueur:</translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.ui" line="82"/>
        <source>IP or Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.ui" line="159"/>
        <source>Remove item</source>
        <translation type="unfinished">Supprimer l&apos;élément</translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.ui" line="164"/>
        <location filename="../dialogs/RemoteDebugDialog.ui" line="167"/>
        <source>Remove all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GDB</source>
        <translation type="vanished">GDB</translation>
    </message>
    <message>
        <source>WinDbg - Pipe</source>
        <translation type="vanished">WinDbg - Pipe</translation>
    </message>
    <message>
        <source>IP:</source>
        <translation type="vanished">IP:</translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.ui" line="75"/>
        <source>Port:</source>
        <translation>Port :</translation>
    </message>
    <message>
        <source>Path:</source>
        <translation type="vanished">Chemin d&apos;accès :</translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.cpp" line="57"/>
        <source>Invalid debugger</source>
        <translation>Débogueur invalide</translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.cpp" line="68"/>
        <source>Invalid IP address</source>
        <translation>Adresse IP invalide</translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.cpp" line="81"/>
        <source>Path does not exist</source>
        <translation>Chemin d&apos;accès inexistant</translation>
    </message>
    <message>
        <location filename="../dialogs/RemoteDebugDialog.cpp" line="94"/>
        <source>Invalid port</source>
        <translation>Port non valide</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <source>Name:</source>
        <translation type="vanished">Nom:</translation>
    </message>
</context>
<context>
    <name>ResourcesModel</name>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="76"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="78"/>
        <source>Vaddr</source>
        <translation>Vaddr</translation>
    </message>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="80"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="82"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="84"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="86"/>
        <source>Lang</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="88"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>ResourcesWidget</name>
    <message>
        <location filename="../widgets/ResourcesWidget.cpp" line="118"/>
        <source>Resources</source>
        <translation>Ressources</translation>
    </message>
</context>
<context>
    <name>RizinGraphWidget</name>
    <message>
        <location filename="../widgets/RizinGraphWidget.ui" line="57"/>
        <source>ag...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="21"/>
        <source>Data reference graph (aga)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="22"/>
        <source>Global data references graph (agA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="26"/>
        <source>Imports graph (agi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="27"/>
        <source>References graph (agr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="28"/>
        <source>Global references graph (agR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="29"/>
        <source>Cross references graph (agx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="30"/>
        <source>Custom graph (agg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/RizinGraphWidget.cpp" line="31"/>
        <source>User command</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RizinPluginsDialog</name>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="14"/>
        <source>Rizin plugin information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="24"/>
        <source>RzBin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="30"/>
        <source>RzBin plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="41"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="82"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="123"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="154"/>
        <source>Name</source>
        <translation type="unfinished">Nom</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="46"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="87"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="128"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="174"/>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="51"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="92"/>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="179"/>
        <source>License</source>
        <translation type="unfinished">Licence</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="56"/>
        <source>Type</source>
        <translation type="unfinished">Type</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="65"/>
        <source>RzIO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="71"/>
        <source>RzIO plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="97"/>
        <source>Permissions</source>
        <translation type="unfinished">Autorisations</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="106"/>
        <source>RzCore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="112"/>
        <source>RzCore plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="137"/>
        <source>RzAsm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="143"/>
        <source>RzAsm plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="159"/>
        <source>Architecture</source>
        <translation type="unfinished">Architecture</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="164"/>
        <source>CPU&apos;s</source>
        <translation type="unfinished">Processeur&apos;s</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="169"/>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinPluginsDialog.ui" line="184"/>
        <source>Author</source>
        <translation type="unfinished">Auteur</translation>
    </message>
</context>
<context>
    <name>RizinTaskDialog</name>
    <message>
        <location filename="../dialogs/RizinTaskDialog.ui" line="14"/>
        <source>Rizin Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinTaskDialog.ui" line="20"/>
        <source>Rizin task in progress..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/RizinTaskDialog.ui" line="27"/>
        <source>Time</source>
        <translation type="unfinished">Temps</translation>
    </message>
    <message>
        <location filename="../dialogs/RizinTaskDialog.cpp" line="32"/>
        <source>Running for</source>
        <translation type="unfinished">Actif depuis</translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/RizinTaskDialog.cpp" line="34"/>
        <source>%n hour</source>
        <comment>%n hours</comment>
        <translation type="unfinished">
            <numerusform>%n heure</numerusform>
            <numerusform>%n heure(s)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/RizinTaskDialog.cpp" line="38"/>
        <source>%n minute</source>
        <comment>%n minutes</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../dialogs/RizinTaskDialog.cpp" line="41"/>
        <source>%n seconds</source>
        <comment>%n second</comment>
        <translation type="unfinished">
            <numerusform>%n seconde(s)</numerusform>
            <numerusform>%n seconde(s)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RunScriptTask</name>
    <message>
        <location filename="../common/RunScriptTask.cpp" line="18"/>
        <source>Executing script...</source>
        <translation>Exécution du script...</translation>
    </message>
    <message>
        <location filename="../common/RunScriptTask.h" line="15"/>
        <source>Run Script</source>
        <translation>Exécuter le script</translation>
    </message>
</context>
<context>
    <name>SaveProjectDialog</name>
    <message>
        <source>Save Project</source>
        <translation type="vanished">Sauvegarder le projet</translation>
    </message>
    <message>
        <source>Project name (prj.name):</source>
        <translation type="vanished">Nom du projet (prj.name):</translation>
    </message>
    <message>
        <source>Projects path (dir.projects):</source>
        <translation type="vanished">Chemin des projets (dir.projects):</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Sélectionner</translation>
    </message>
    <message>
        <source>Use simple project saving style (prj.simple, recommended)</source>
        <translation type="vanished">Utiliser un style de sauvegarde simple du projet (prj.simple, recommandé)</translation>
    </message>
    <message>
        <source>Save the target binary inside the project directory (prj.files)</source>
        <translation type="vanished">Enregistrer le binaire cible dans le répertoire du projet (prj.files)</translation>
    </message>
    <message>
        <source>Project is a git repo and saving is committing (prj.git)</source>
        <translation type="vanished">Le projet est un dépôt git et l&apos;enregistrement est en cours de validation (prj.git)</translation>
    </message>
    <message>
        <source>Use ZIP format for project files (prj.zip)</source>
        <translation type="vanished">Utiliser le format ZIP pour les fichiers du projet (prj.zip)</translation>
    </message>
    <message>
        <source>Select project path (dir.projects)</source>
        <translation type="vanished">Sélectionner le chemin du projet (dir.projects)</translation>
    </message>
    <message>
        <source>Save project</source>
        <translation type="vanished">Sauvegarder le projet</translation>
    </message>
    <message>
        <source>Invalid project name.</source>
        <translation type="vanished">Nom de projet invalide.</translation>
    </message>
</context>
<context>
    <name>SdbDock</name>
    <message>
        <source>Key</source>
        <translation type="obsolete">Key</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valeur</translation>
    </message>
</context>
<context>
    <name>SdbWidget</name>
    <message>
        <location filename="../widgets/SdbWidget.ui" line="17"/>
        <source>SDB Browser</source>
        <translation>Navigateur SDB</translation>
    </message>
    <message>
        <location filename="../widgets/SdbWidget.ui" line="120"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../widgets/SdbWidget.ui" line="125"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>SearchModel</name>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="96"/>
        <source>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Preview&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</source>
        <translation>&lt;div style=&quot;margin-bottom: 10px;&quot;&gt;&lt;strong&gt;Aperçu&lt;/strong&gt;:&lt;br&gt;%1&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="115"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="117"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="119"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="121"/>
        <source>Data</source>
        <translation>Données</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="123"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../widgets/SearchWidget.ui" line="83"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.ui" line="90"/>
        <source>Search for:</source>
        <translation>Rechercher :</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.ui" line="100"/>
        <source>Search in:</source>
        <translation>Rechercher dans :</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="251"/>
        <source>asm code</source>
        <translation>code asm</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="252"/>
        <source>string</source>
        <translation>chaine de caractères</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="253"/>
        <source>hex string</source>
        <translation>chaîne hexadécimale</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="254"/>
        <source>ROP gadgets</source>
        <translation>ROP gadgets</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="255"/>
        <source>32bit value</source>
        <translation>Valeur 32 bits</translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="282"/>
        <source>No results found for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/SearchWidget.cpp" line="285"/>
        <source>No Results Found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SectionsModel</name>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="93"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="95"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="101"/>
        <source>Virtual Size</source>
        <translation>Taille virtuelle</translation>
    </message>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="97"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="99"/>
        <source>End Address</source>
        <translation>Adresse de fin</translation>
    </message>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="103"/>
        <source>Permissions</source>
        <translation>Autorisations</translation>
    </message>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="105"/>
        <source>Entropy</source>
        <translation>Entropie</translation>
    </message>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="107"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>SegmentsModel</name>
    <message>
        <location filename="../widgets/SegmentsWidget.cpp" line="81"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/SegmentsWidget.cpp" line="83"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/SegmentsWidget.cpp" line="85"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../widgets/SegmentsWidget.cpp" line="87"/>
        <source>End Address</source>
        <translation>Adresse de fin</translation>
    </message>
    <message>
        <location filename="../widgets/SegmentsWidget.cpp" line="89"/>
        <source>Permissions</source>
        <translation>Autorisations</translation>
    </message>
    <message>
        <location filename="../widgets/SegmentsWidget.cpp" line="91"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>SetFunctionVarTypes</name>
    <message>
        <source>Dialog</source>
        <translation type="obsolete">Dialog</translation>
    </message>
    <message>
        <source>Set Type To:</source>
        <translation type="obsolete">Set Type To:</translation>
    </message>
    <message>
        <source>Set Name To:</source>
        <translation type="obsolete">Set Name To:</translation>
    </message>
    <message>
        <source>Modify:</source>
        <translation type="obsolete">Modify:</translation>
    </message>
    <message>
        <source>You must be in a function to define variable types.</source>
        <translation type="obsolete">You must be in a function to define variable types.</translation>
    </message>
</context>
<context>
    <name>SetToDataDialog</name>
    <message>
        <location filename="../dialogs/SetToDataDialog.ui" line="14"/>
        <source>Set to Data</source>
        <translation>Définir comme données</translation>
    </message>
    <message>
        <location filename="../dialogs/SetToDataDialog.ui" line="20"/>
        <location filename="../dialogs/SetToDataDialog.ui" line="34"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../dialogs/SetToDataDialog.ui" line="27"/>
        <source>Start address</source>
        <translation>Adresse de départ</translation>
    </message>
    <message>
        <location filename="../dialogs/SetToDataDialog.ui" line="41"/>
        <source>End address</source>
        <translation>Adresse de fin</translation>
    </message>
    <message>
        <location filename="../dialogs/SetToDataDialog.ui" line="48"/>
        <source>Item size</source>
        <translation>Taille de l&apos;élément</translation>
    </message>
    <message>
        <location filename="../dialogs/SetToDataDialog.ui" line="55"/>
        <source>Number of items</source>
        <translation>Nombre d&apos;éléments</translation>
    </message>
    <message>
        <location filename="../dialogs/SetToDataDialog.ui" line="62"/>
        <location filename="../dialogs/SetToDataDialog.ui" line="69"/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Form</translation>
    </message>
    <message>
        <source>Script</source>
        <translation type="obsolete">Script</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="vanished">X</translation>
    </message>
    <message>
        <source>example.py</source>
        <translation type="obsolete">example.py</translation>
    </message>
    <message>
        <source>Execution finished</source>
        <translation type="obsolete">Execution finished</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation type="obsolete">Calculator</translation>
    </message>
    <message>
        <source>Assembler</source>
        <translation type="obsolete">Assembler</translation>
    </message>
    <message>
        <source>Assembly</source>
        <translation type="obsolete">Assembly</translation>
    </message>
    <message>
        <source>v</source>
        <translation type="obsolete">v</translation>
    </message>
    <message>
        <source>^</source>
        <translation type="obsolete">^</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation type="vanished">Hexadécimal</translation>
    </message>
    <message>
        <source>Toogle resposiveness</source>
        <translation type="obsolete">Toogle resposiveness</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Settings</translation>
    </message>
</context>
<context>
    <name>SidebarWidget</name>
    <message>
        <source> Function:</source>
        <translation type="obsolete"> Function:</translation>
    </message>
    <message>
        <source>Offset info:</source>
        <translation type="obsolete">Offset info:</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="vanished">Info</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valeur</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Opcode description:</source>
        <translation type="obsolete">Opcode description:</translation>
    </message>
    <message>
        <source>Function registers info:</source>
        <translation type="obsolete">Function registers info:</translation>
    </message>
    <message>
        <source>X-Refs to current address:</source>
        <translation type="obsolete">X-Refs to current address:</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="vanished">Adresse</translation>
    </message>
    <message>
        <source>Instruction</source>
        <translation type="vanished">Instruction</translation>
    </message>
    <message>
        <source>X-Refs from current address:</source>
        <translation type="obsolete">X-Refs from current address:</translation>
    </message>
</context>
<context>
    <name>SimpleTextGraphView</name>
    <message>
        <location filename="../widgets/SimpleTextGraphView.cpp" line="29"/>
        <source>Copy</source>
        <translation type="unfinished">Copier</translation>
    </message>
</context>
<context>
    <name>StackModel</name>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="216"/>
        <source>Offset</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="218"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="220"/>
        <source>Reference</source>
        <translation>Référence</translation>
    </message>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="222"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>StackWidget</name>
    <message>
        <source>Offset</source>
        <translation type="vanished">Décalage</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="vanished">Valeur</translation>
    </message>
    <message>
        <source>Reference</source>
        <translation type="obsolete">Reference</translation>
    </message>
    <message>
        <source>Seek to this offset</source>
        <translation type="obsolete">Seek to this offset</translation>
    </message>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="33"/>
        <source>Edit stack value...</source>
        <translation>Modifier la valeur de la stack...</translation>
    </message>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="109"/>
        <source>Edit stack at %1</source>
        <translation>Modifier la pile à %1</translation>
    </message>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="138"/>
        <source>Stack position</source>
        <translation>Position de la pile</translation>
    </message>
    <message>
        <location filename="../widgets/StackWidget.cpp" line="140"/>
        <source>Pointed memory</source>
        <translation>Mémoire pointée</translation>
    </message>
</context>
<context>
    <name>StringsModel</name>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="67"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="69"/>
        <source>String</source>
        <translation>Chaine de caractères</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="71"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="73"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="75"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="77"/>
        <source>Section</source>
        <translation>Section</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="79"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>StringsTask</name>
    <message>
        <location filename="../common/StringsTask.h" line="13"/>
        <source>Searching for Strings</source>
        <translation>Rechercher des chaines de caractères</translation>
    </message>
</context>
<context>
    <name>StringsWidget</name>
    <message>
        <source>Copy Address</source>
        <translation type="obsolete">Copy Address</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.ui" line="76"/>
        <source>Copy String</source>
        <translation>Copier la chaine de caractères</translation>
    </message>
    <message>
        <source>Xrefs</source>
        <translation type="obsolete">Xrefs</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.ui" line="81"/>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="161"/>
        <source>Section:</source>
        <translation>Section:</translation>
    </message>
    <message>
        <location filename="../widgets/StringsWidget.cpp" line="244"/>
        <source>(all)</source>
        <translation>(tous)</translation>
    </message>
</context>
<context>
    <name>SymbolsModel</name>
    <message>
        <location filename="../widgets/SymbolsWidget.cpp" line="58"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../widgets/SymbolsWidget.cpp" line="60"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../widgets/SymbolsWidget.cpp" line="62"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/SymbolsWidget.cpp" line="64"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>SymbolsWidget</name>
    <message>
        <location filename="../widgets/SymbolsWidget.cpp" line="123"/>
        <source>Symbols</source>
        <translation>Symboles</translation>
    </message>
</context>
<context>
    <name>ThreadsWidget</name>
    <message>
        <location filename="../widgets/ThreadsWidget.cpp" line="24"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../widgets/ThreadsWidget.cpp" line="25"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../widgets/ThreadsWidget.cpp" line="26"/>
        <source>Path</source>
        <translation>Chemin d&apos;accès</translation>
    </message>
</context>
<context>
    <name>TypesInteractionDialog</name>
    <message>
        <location filename="../dialogs/TypesInteractionDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../dialogs/TypesInteractionDialog.ui" line="23"/>
        <source>Load From File:</source>
        <translation>Charger depuis un fichier:</translation>
    </message>
    <message>
        <location filename="../dialogs/TypesInteractionDialog.ui" line="37"/>
        <source>Select File</source>
        <translation>Sélectionner le fichier</translation>
    </message>
    <message>
        <location filename="../dialogs/TypesInteractionDialog.ui" line="50"/>
        <source>Enter Types Manually</source>
        <translation>Entrer des types manuellement</translation>
    </message>
    <message>
        <location filename="../dialogs/TypesInteractionDialog.cpp" line="32"/>
        <source>Select file</source>
        <translation>Sélectionner le fichier</translation>
    </message>
    <message>
        <location filename="../dialogs/TypesInteractionDialog.cpp" line="40"/>
        <location filename="../dialogs/TypesInteractionDialog.cpp" line="68"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/TypesInteractionDialog.cpp" line="69"/>
        <source>There was some error while loading new types</source>
        <translation>Il y a eu des erreurs en chargeant de nouveaux types</translation>
    </message>
</context>
<context>
    <name>TypesModel</name>
    <message>
        <source>Type</source>
        <translation type="vanished">Type</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="62"/>
        <source>Type / Name</source>
        <translation>Type / Nom</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="64"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="66"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="68"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
</context>
<context>
    <name>TypesWidget</name>
    <message>
        <location filename="../widgets/TypesWidget.ui" line="76"/>
        <location filename="../widgets/TypesWidget.ui" line="79"/>
        <source>Export Types</source>
        <translation>Exporter les types</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.ui" line="84"/>
        <location filename="../widgets/TypesWidget.ui" line="87"/>
        <location filename="../widgets/TypesWidget.cpp" line="286"/>
        <source>Load New Types</source>
        <translation>Charger de nouveaux types</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.ui" line="92"/>
        <location filename="../widgets/TypesWidget.ui" line="95"/>
        <source>Delete Type</source>
        <translation>Supprimez le type</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.ui" line="100"/>
        <source>Link Type to Address</source>
        <translation>Type de lien vers l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="140"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="185"/>
        <source>View Type</source>
        <translation>Voir le type</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="186"/>
        <source>Edit Type</source>
        <translation>Modifier le type</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="217"/>
        <source>(All)</source>
        <translation>(Tous)</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="266"/>
        <source>Save File</source>
        <translation>Sauvegarder le fichier</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="273"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="302"/>
        <source>Edit Type: </source>
        <translation>Modifier le type: </translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../widgets/TypesWidget.cpp" line="355"/>
        <source>View Type: </source>
        <translation>Voir le type: </translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="305"/>
        <location filename="../widgets/TypesWidget.cpp" line="355"/>
        <source> (Read Only)</source>
        <translation> (Lecture Seule)</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="324"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../widgets/TypesWidget.cpp" line="324"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation>Êtes-vous sûrs de vouloir supprimer &quot;%1&quot;?</translation>
    </message>
</context>
<context>
    <name>UpdateWorker</name>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="34"/>
        <source>Time limit exceeded during version check. Please check your internet connection and try again.</source>
        <translation>La limite de temps à été dépassée pendant la vérification de la version. Veuillez vérifier votre connexion internet et réessayer.</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="81"/>
        <source>Version control</source>
        <translation>Contrôle de version</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="82"/>
        <source>There is an update available for Cutter.&lt;br/&gt;</source>
        <translation>Il y a une mise à jour de Cutter disponible.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="82"/>
        <source>Current version:</source>
        <translation>Version actuelle:</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="83"/>
        <source>Latest version:</source>
        <translation>Dernière version:</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="85"/>
        <source>For update, please check the link:&lt;br/&gt;</source>
        <translation>Pour des mises à jour, visitez le lien:&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="89"/>
        <source>or click &quot;Download&quot; to download latest version of Cutter.</source>
        <translation>ou cliquez sur &quot;Télécharger&quot; pour télécharger la dernière version de Cutter.</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="92"/>
        <source>Don&apos;t check for updates</source>
        <translation>Ne pas vérifier la présence de mises à jour</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="96"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="103"/>
        <source>Choose directory for downloading</source>
        <translation>Choisissez un répertoire pour le téléchargement</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="108"/>
        <source>Downloading update...</source>
        <translation>Téléchargement de la mise à jour...</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="108"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="116"/>
        <source>Download finished!</source>
        <translation>Téléchargement terminé!</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="117"/>
        <source>Latest version of Cutter was succesfully downloaded!</source>
        <translation>La dernière version de Cutter a été téléchargée avec succès!</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="119"/>
        <source>Open file</source>
        <translation>Ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../common/UpdateWorker.cpp" line="120"/>
        <source>Open download folder</source>
        <translation>Ouvrir le dossier de téléchargement</translation>
    </message>
</context>
<context>
    <name>VTableModel</name>
    <message>
        <location filename="../widgets/VTablesWidget.cpp" line="63"/>
        <source>VTable</source>
        <translation>Table virtuelle</translation>
    </message>
    <message>
        <location filename="../widgets/VTablesWidget.cpp" line="84"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/VTablesWidget.cpp" line="86"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
</context>
<context>
    <name>VersionInfoDialog</name>
    <message>
        <location filename="../dialogs/VersionInfoDialog.ui" line="50"/>
        <location filename="../dialogs/VersionInfoDialog.ui" line="64"/>
        <source>TextLabel</source>
        <translation>Étiquette de texte</translation>
    </message>
    <message>
        <location filename="../dialogs/VersionInfoDialog.ui" line="94"/>
        <location filename="../dialogs/VersionInfoDialog.ui" line="126"/>
        <source>Key</source>
        <translation>Clé</translation>
    </message>
    <message>
        <location filename="../dialogs/VersionInfoDialog.ui" line="99"/>
        <location filename="../dialogs/VersionInfoDialog.ui" line="131"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>VirtualAddrDock</name>
    <message>
        <location filename="../widgets/SectionsWidget.cpp" line="508"/>
        <source>Virtual</source>
        <translation>Virtuel</translation>
    </message>
</context>
<context>
    <name>VisualNavbar</name>
    <message>
        <location filename="../widgets/VisualNavbar.cpp" line="29"/>
        <source>Visual navigation bar</source>
        <translation>Barre de navigation visuelle</translation>
    </message>
</context>
<context>
    <name>WelcomeDialog</name>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="20"/>
        <source>Welcome to Cutter</source>
        <translation>Bienvenue dans Cutter</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="83"/>
        <source>Cutter</source>
        <translation>Cutter</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="104"/>
        <location filename="../dialogs/WelcomeDialog.cpp" line="19"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="168"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="203"/>
        <source>Native Theme</source>
        <translation>Thème natif</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="208"/>
        <source>Dark Theme</source>
        <translation>Thème Sombre</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="213"/>
        <source>Midnight Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="218"/>
        <source>Light Theme</source>
        <translation>Thème clair</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="229"/>
        <source>Check for updates on start</source>
        <translation>Vérifier les mises à jour au démarrage</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="298"/>
        <source>Community</source>
        <translation>Communauté</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="320"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/cutter_re&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@cutter_re &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="355"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/rizinorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;Rejoignez des milliers d&apos;ingénieur en RE dans notre communauté:&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;	&lt;/span&gt;&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;#cutter on &lt;/span&gt;&lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; and report bugs or contribute code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Vous voulez nous aider à améliorer Cutter ?&lt;br/&gt;Visitez notre &lt;/span&gt;&lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#2980b9;&quot;&gt;page Github&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; et rapportez des bugs ou contribuez au code.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="417"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Join thousands of reverse engineers in our community:&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Twitter:&lt;/span&gt;	&lt;a href=&quot;https://twitter.com/r2gui&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2gui&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Telegram:	&lt;/span&gt;&lt;a href=&quot;https://t.me/r2cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;@r2cutter &lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;IRC:	&lt;/span&gt;#cutter on &lt;a href=&quot;irc.freenode.net&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;irc.freenode.net&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Want to help us make Cutter even better?&lt;br/&gt;Visit our &lt;a href=&quot;https://github.com/radareorg/cutter&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Github page&lt;/span&gt;&lt;/a&gt; and report bugs or contribute code.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.ui" line="394"/>
        <source>Contributing</source>
        <translation>Contribuant</translation>
    </message>
    <message>
        <source>Continue 🢒</source>
        <translation type="obsolete">Continue 🢒</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.cpp" line="70"/>
        <source>Language settings</source>
        <translation>Paramètres de langue</translation>
    </message>
    <message>
        <location filename="../dialogs/WelcomeDialog.cpp" line="71"/>
        <source>Language will be changed after next application start.</source>
        <translation>Le langage sera modifiée après le prochain lancement de l&apos;application.</translation>
    </message>
</context>
<context>
    <name>XrefModel</name>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="287"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="291"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="293"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="289"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
</context>
<context>
    <name>XrefsDialog</name>
    <message>
        <source>Address</source>
        <translation type="vanished">Adresse</translation>
    </message>
    <message>
        <source>Code</source>
        <translation type="obsolete">Code</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Type</translation>
    </message>
    <message>
        <source>X-Refs to %1:</source>
        <translation type="vanished">Références croisées vers %1:</translation>
    </message>
    <message>
        <source>X-Refs from %1:</source>
        <translation type="vanished">Références croisées de %1:</translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="145"/>
        <source>X-Refs to %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="147"/>
        <source>X-Refs from %1 (%2 results):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="152"/>
        <source>Writes to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="153"/>
        <source>Reads from %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/XrefsDialog.cpp" line="164"/>
        <location filename="../dialogs/XrefsDialog.cpp" line="183"/>
        <source>X-Refs for %1</source>
        <translation>Références croisées pour %1</translation>
    </message>
</context>
<context>
    <name>ZignaturesModel</name>
    <message>
        <location filename="../widgets/ZignaturesWidget.cpp" line="66"/>
        <source>Offset</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location filename="../widgets/ZignaturesWidget.cpp" line="68"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../widgets/ZignaturesWidget.cpp" line="70"/>
        <source>Bytes</source>
        <translation>Offset</translation>
    </message>
</context>
</TS>
